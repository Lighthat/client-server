#include "mainwindow.h"
#include "client.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    if (w.client.validated){
        w.show();
        return a.exec();
    }
    else
        return 0;

}
