#include "inventorywindow.h"
#include "ui_inventorywindow.h"
#include "client.h"

InventoryWindow::InventoryWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::InventoryWindow)
{
    ui->setupUi(this);
    this->setWindowFlags((
    (windowFlags() | Qt::CustomizeWindowHint)
    & ~Qt::WindowCloseButtonHint
    ));
}

InventoryWindow::~InventoryWindow()
{
    emit inventoryClosed();
    delete ui;
}

void InventoryWindow::on_exitInventory_clicked()
{
    close();
}

void InventoryWindow::getPlayer(Player sentPlayer){
    player = sentPlayer;
    ui->moneyValLabel->setText(QString::number(player.money));
    ui->goldValLabel->setText(QString::number(player.gold));
    ui->ironValLabel->setText(QString::number(player.iron));
    ui->shardsValLabel->setText(QString::number(player.shards));
    ui->siliconValLabel->setText(QString::number(player.silicon));
    ui->sulfurValLabel->setText(QString::number(player.sulfur));
}
