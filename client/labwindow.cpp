#include "labwindow.h"
#include "ui_labwindow.h"

LabWindow::LabWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LabWindow)
{
    ui->setupUi(this);
}

LabWindow::~LabWindow()
{
    delete ui;
}
