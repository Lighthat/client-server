#include "client.h"
#include "string.h"
#include <QHostAddress>
#include <iostream>
#include <QDebug>
#include <QMessageBox>
#include <stdint.h>
#include <QtEndian>

Client::Client(QObject* parent): QObject(parent)
{
    connect(&client, SIGNAL(connected()),
      this, SLOT(startTransfer()));
    connect(&client, SIGNAL(readyRead()),
      this, SLOT(getHeader()));
    connect(this, SIGNAL(hasData(QByteArray)),
      this, SLOT(acceptResponse(QByteArray)));

    timeoutTimer = 0;
}

Client::~Client()
{
  client.close();
}

void Client::sendDisconnection(){
    dataToSend data;
    QByteArray clientSendBuf;
    data.activityID = 4;
    data.stringData << "Connection closed.";
    data.stringCount = 1;
    data.stringLength.append(data.stringData.at(0).length());
    clientSendBuf.append( (char) data.activityID);
    clientSendBuf.append( (char) data.stringCount);
    clientSendBuf.append( (char) data.stringLength[0]);
    for (int i = 0; i < data.stringData[0].length(); i++){
      clientSendBuf.append(data.stringData[0].at(i))   ;
    }
    // Header
    uint16_t messageSize = clientSendBuf.size();
    char *bytes((char*)&messageSize);
    clientSendBuf.prepend(bytes, sizeof(messageSize));
    client.write(clientSendBuf);
}

void Client::featurePlaceholder(){
    QMessageBox msgBox;
    msgBox.setText("Sorry, this feature hasn't been implemented yet!");
    msgBox.setWindowTitle("Error");
    msgBox.setIcon(QMessageBox::Information);
    msgBox.exec();
}

void Client::acceptResponse(QByteArray data){
        char clientReceiveBuf[65536];
        int messageSize;
        memcpy(&messageSize, data, 2);
        bool haveAllData = false;

        forever {
            qint64 bytes = client.bytesAvailable();
            if (bytes < messageSize) break;
            client.read(clientReceiveBuf, messageSize);
            haveAllData = true;
            break;
        }
        if(haveAllData) readData(clientReceiveBuf);
        //emit doneReading(clientReceiveBuf);
}

void Client::getHeader(){
    // TCP/IP data might come in arbitrarily sized chunks, we can only process it
    // once it's all here.

    forever {
        int N = sizeof(uint16_t);
        qint64 bytes = client.bytesAvailable();
        if (bytes < N) break;
        QByteArray buf = client.read(N);
        emit hasData(buf);
    }
}

void Client::readData(std::string clientReceiveBuf){
    dataToSend data;
    QString stringBuilder;
    data.activityID = clientReceiveBuf.at(0);
    data.stringCount = clientReceiveBuf.at(1);
    int i,
        j = 2, //placeholder for next slot to be used in the receive buffer
        k = 0;
    for (i = 0; i < data.stringCount; i++){ //while there are strings whose lengths we need to document
        data.stringLength.insert(i, clientReceiveBuf.at(2+i));
        j++;
    }
    while(k < data.stringCount){ //while there are strings in the buffer to be put into the data structure
        i = 0;
        while(i < data.stringLength.at(k)){ //while there are characters in the buffer to be built into a string
            stringBuilder.append(clientReceiveBuf.at(j));
            j++;
            i++;
        }
        //we have assembled a string from chars in the buffer
        //now we put them into the struct
        data.stringData << stringBuilder;
        stringBuilder = ""; //empty string builder for next string.
        k++;
    }

    //Handle based on activity ID
    if (data.activityID == 1){
        validated = true;
        qDebug() << "Activity ID: 1";
        return;
    }
    else if (data.activityID == 2){
        validated = false;
        qDebug() << "Activity ID: 2";
        return;
    }
    else if (data.activityID == 3){
        player.level = data.stringData.at(0).toInt();
        player.experience = data.stringData.at(1).toInt();
        player.strength = data.stringData.at(2).toInt();
        player.guile = data.stringData.at(3).toInt();
        player.intelligence = data.stringData.at(4).toInt();
        player.resilience = data.stringData.at(5).toInt();
        player.statpoints = data.stringData.at(6).toInt();
        player.resetpoints = data.stringData.at(7).toInt();
        player.miningattempts = data.stringData.at(8).toInt();
        player.miningupgrades = data.stringData.at(9).toInt();
        player.money = data.stringData.at(10).toInt();
        player.maxHP = data.stringData.at(11).toInt();
        player.currentHP = data.stringData.at(12).toInt();
        player.maxMana = data.stringData.at(13).toInt();
        player.currentMana = data.stringData.at(14).toInt();
        player.gold = data.stringData.at(15).toInt();
        player.shards = data.stringData.at(16).toInt();
        player.iron = data.stringData.at(17).toInt();
        player.silicon = data.stringData.at(18).toInt();
        player.sulfur = data.stringData.at(19).toInt();
        player.goldLevel = data.stringData.at(20).toInt();
        player.shardsLevel = data.stringData.at(21).toInt();
        player.ironLevel = data.stringData.at(22).toInt();
        player.siliconLevel = data.stringData.at(23).toInt();
        player.sulfurLevel = data.stringData.at(24).toInt();
        player.energy = data.stringData.at(25).toDouble();
        player.maxEnergy = data.stringData.at(26).toDouble();
        validated = true;
        qDebug() << "Activity ID: 3";
        return;
    }
    else if (data.activityID == 4){
        validated = false;
        qDebug() << "Activity ID: 4";
        return;
    }
    else if (data.activityID == 5){
        validated = false;
        qDebug() << "Activity ID: 5";
        return;
    }
    else if (data.activityID == 6){
        player.miningattempts = data.stringData.at(0).toInt();
        player.gold = data.stringData.at(1).toInt();
        player.shards = data.stringData.at(2).toInt();
        player.iron = data.stringData.at(3).toInt();
        player.silicon = data.stringData.at(4).toInt();
        player.sulfur = data.stringData.at(5).toInt();
        emit actionSuccess(6);
        return;
    }
    else if (data.activityID == 7){
        emit actionFailed(7);
        return;
    }
    else if (data.activityID == 8){
        player.miningattempts = data.stringData.at(0).toInt();
        player.energy = data.stringData.at(1).toDouble();
        emit actionSuccess(8);
        return;
    }
    else if (data.activityID == 9){
        timeoutTimer = 0;
        return;
    }
    else if (data.activityID == 10){
        QMessageBox msgBox;
        msgBox.setText("Failed to log in\nuser is already logged in.");
        msgBox.exec();
        std::exit(0);
    }
    else if(data.activityID == 11){
        QMessageBox msgBox;
        msgBox.setText("Not enough energy to go on an adventure.");
        msgBox.exec();
        delay(300);
        emit actionFailed(0);
        return;
    }
    else if(data.activityID == 36){
        QMessageBox msgBox;
        msgBox.setText("You can't go on an adventure when you're dead.");
        msgBox.exec();
        delay(300);
        emit actionFailed(1);
        return;
    }

}

void Client::start(QString address, quint16 port)
{
  QHostAddress addr(address);
  client.connectToHost(addr, port);
}

void Client::startTransfer()
{
    QByteArray clientSendBuf;
    dataToSend data;
    QString greetingMessage = "Incoming connection...";
    data.activityID = 1;
    data.stringData << encrypt(greetingMessage);
    data.stringCount = 1;
    data.stringLength.append(data.stringData.at(0).length()); //populating the string length array
    clientSendBuf.append( (char) data.activityID); //populating the send buffer
    clientSendBuf.append( (char) data.stringCount);
    clientSendBuf.append( (char) data.stringLength[0]);
    for (int j = 0; j < data.stringLength[0]; j++){
        clientSendBuf.append(data.stringData[0].at(j));
    }


    // Header
    uint16_t messageSize = clientSendBuf.size();
    char *bytes((char*)&messageSize);
    clientSendBuf.prepend(bytes, sizeof(messageSize));

    client.write(clientSendBuf);
}

void Client::delay( int millisecondsToWait )
{
    QTime dieTime = QTime::currentTime().addMSecs( millisecondsToWait );
    while( QTime::currentTime() < dieTime )
    {
        QCoreApplication::processEvents( QEventLoop::AllEvents, 100 );
    }
}

bool Client::login(QString username, QString password){
    QByteArray clientSendBuf;
    dataToSend data;
    //QString greetingMessage = "Incoming connection...";
    data.activityID = 2;
    //Username & password will come encrypted.
    data.stringData << username;
    data.stringData << password;
    data.stringCount = 2;
    data.stringLength.append(data.stringData.at(0).length()); //populating the string length array
    data.stringLength.append(data.stringData.at(1).length());
    clientSendBuf.append( (char) data.activityID); //populating the send buffer
    clientSendBuf.append( (char) data.stringCount);
    clientSendBuf.append( (char) data.stringLength[0]);
    clientSendBuf.append( (char) data.stringLength[1]);
    for (int i = 0; i < data.stringData[0].length(); i++){
        clientSendBuf.append(data.stringData[0].at(i));
    }
    for (int i = 0; i < data.stringData[1].length(); i++){
        clientSendBuf.append(data.stringData[1].at(i));
    }

    // Header
    uint16_t messageSize = clientSendBuf.size();
    char *bytes((char*)&messageSize);
    clientSendBuf.prepend(bytes, sizeof(messageSize));

    client.write(clientSendBuf);
    if(client.waitForReadyRead(15000)){
        if(validated == true)
            return true;
        else
            return false;
    }
    else{
        return false;
    }
}

bool Client::getLoginInfo(){
    bool userok = false;
    bool passok = true;
    bool loginok = false;

    QString username, password;
    QMessageBox msgBox;

    while(!loginok){
        username = QInputDialog::getText(NULL, "Login",
                                             "Username:", QLineEdit::Normal,
                                              NULL, &userok);
        if(!userok){
            msgBox.setText("Login cancelled.");
            msgBox.exec();
            sendDisconnection();
            delay(300);
            std::exit(0);
        }
        password = QInputDialog::getText(NULL, "Login",
                                        "Password:", QLineEdit::Password,
                                         NULL, &passok);

        if (!passok){
            msgBox.setText("Login cancelled.");
            msgBox.exec();
            sendDisconnection();
            delay(300);
            std::exit(0);
        }
        else if (username.isEmpty() || password.isEmpty()){
            msgBox.setText("Invalid entry for username or password.");
            msgBox.exec();
        }
        else{
            QString hashedUsername = encrypt(username);
            QString hashedPassword = encrypt(password);
            loginok = login(hashedUsername, hashedPassword);
            if(!loginok){
                msgBox.setText("Login failed. Maybe the connection timed out, or maybe your information was incorrect.")   ;
                msgBox.exec();
            }
            else
                player.username = username;
        }
    }
    return true;
}

bool Client::initialRun(){
    QMessageBox msgBox(QMessageBox::Question,
                       "Welcome",
                       "What would you like to do?",
                       QMessageBox::Yes | QMessageBox::No);
    msgBox.setButtonText(QMessageBox::Yes, "Log in");
    msgBox.setButtonText(QMessageBox::No, "Register");


    if (msgBox.exec() == QMessageBox::Yes) {
        if(getLoginInfo())
            return true;
        else
            return false;
    }
    else{
        if(getRegistrationInfo())
            return true;
        else
            return false;
    }
    return false;
}

bool Client::getRegistrationInfo(){
    bool userok = false;
    bool passok = true;
    bool regok = false;

    QString username, password;
    QMessageBox msgBox;

    while(!regok){
        username = QInputDialog::getText(NULL, "Registration",
                                             "Choose a username:", QLineEdit::Normal,
                                              NULL, &userok);
        if(!userok){
            msgBox.setText("Registration cancelled.");
            msgBox.exec();
            return false;
        }
        password = QInputDialog::getText(NULL, "Registration",
                                        "Choose a password:", QLineEdit::Password,
                                         NULL, &passok);

        if (!passok){
            msgBox.setText("Registration cancelled.");
            msgBox.exec();
            return false;
        }
        else if (username.isEmpty() || password.isEmpty()){
            msgBox.setText("Invalid entry for username or password.");
            msgBox.exec();
        }
        else{
            regok = true;
            QString hashedUsername = encrypt(username);
            QString hashedPassword = encrypt(password);
            player.username = username;
            registration(hashedUsername, hashedPassword);
            return true;
        }
    }
    return false;
}

void Client::performAction(int activityID, QStringList stringData, int stringCount){
    QByteArray clientSendBuf;
    dataToSend data;
    data.activityID = activityID;
    for(int i = 0; i < stringCount; i++){
        data.stringData << stringData.at(i);
    }
    data.stringCount = stringCount;
    for(int i = 0; i < stringCount; i++){
        data.stringLength.append(stringData.at(i).length());
    }
    clientSendBuf.append( (char) data.activityID); //populating the send buffer
    clientSendBuf.append( (char) data.stringCount);
    for (int i = 0; i < data.stringLength.size(); i++){
        clientSendBuf.append( (char) data.stringLength[i]);
    }
    for (int j = 0; j < stringCount; j++){
        for (int i = 0; i < data.stringData[j].length(); i++){
            clientSendBuf.append(data.stringData[j].at(i));
        }
    }

    // Header
    uint16_t messageSize = clientSendBuf.size();
    char *bytes((char*)&messageSize);
    clientSendBuf.prepend(bytes, sizeof(messageSize));

    client.write(clientSendBuf);
}

void Client::performAction(int activityID, QString stringData){
    QByteArray clientSendBuf;
    dataToSend data;
    data.activityID = activityID;
    data.stringData << encrypt(player.username) << stringData;
    data.stringCount = 2;
    data.stringLength.append(player.username.length());
    data.stringLength.append(stringData.length());
    clientSendBuf.append( (char) data.activityID); //populating the send buffer
    clientSendBuf.append( (char) data.stringCount);
    clientSendBuf.append( (char) data.stringLength[0]);
    clientSendBuf.append( (char) data.stringLength[1]);
    for (int i = 0; i < data.stringData[0].length(); i++){
        clientSendBuf.append(data.stringData[0].at(i));
    }
    for (int i = 0; i < data.stringData[1].length(); i++){
        clientSendBuf.append(data.stringData[1].at(i));
    }

    // Header
    uint16_t messageSize = clientSendBuf.size();
    char *bytes((char*)&messageSize);
    clientSendBuf.prepend(bytes, sizeof(messageSize));

    client.write(clientSendBuf);
}

void Client::performAction(int activityID){
    QByteArray clientSendBuf;
    dataToSend data;
    data.activityID = activityID;
    data.stringCount = 1;
    data.stringData << encrypt(player.username);
    data.stringLength.append(player.username.length());
    clientSendBuf.append( (char) data.activityID); //populating the send buffer
    clientSendBuf.append( (char) data.stringCount);
    clientSendBuf.append( (char) data.stringLength[0]);
    for (int i = 0; i < data.stringData[0].length(); i++){
        clientSendBuf.append(data.stringData[0].at(i));
    }

    // Header
    uint16_t messageSize = clientSendBuf.size();
    char *bytes((char*)&messageSize);
    clientSendBuf.prepend(bytes, sizeof(messageSize));

    client.write(clientSendBuf);
}

void Client::registration(QString username, QString password){
    QByteArray clientSendBuf;
    dataToSend data;
    QMessageBox msgBox;
    data.activityID = 3;
    //Username & password will come encrypted.
    data.stringData << username;
    data.stringData << password;
    data.stringCount = 2;
    data.stringLength.append(data.stringData.at(0).length()); //populating the string length array
    data.stringLength.append(data.stringData.at(1).length());
    clientSendBuf.append( (char) data.activityID); //populating the send buffer
    clientSendBuf.append( (char) data.stringCount);
    clientSendBuf.append( (char) data.stringLength[0]);
    clientSendBuf.append( (char) data.stringLength[1]);
    for (int i = 0; i < data.stringData[0].length(); i++){
        clientSendBuf.append(data.stringData[0].at(i));
    }
    for (int i = 0; i < data.stringData[1].length(); i++){
        clientSendBuf.append(data.stringData[1].at(i));
    }
    // Header
    uint16_t messageSize = clientSendBuf.size();
    char *bytes((char*)&messageSize);
    clientSendBuf.prepend(bytes, sizeof(messageSize));

    client.write(clientSendBuf);
    if(client.waitForReadyRead(15000)){
        if(validated == true){
            msgBox.setText("Registration Successful");
            msgBox.exec();

            player.experience = 0;
            player.guile = 0;
            player.intelligence = 0;
            player.strength = 0;
            player.resilience = 0;
            player.level = 1;
            player.miningattempts = 5;
            player.miningupgrades = 2;
            player.resetpoints = 5;
            player.statpoints = 15;
            player.money = 250;
            player.maxHP = 175;
            player.currentHP = 175;
            player.maxMana = 50;
            player.currentMana = 50;
            player.gold = 0;
            player.shards = 0;
            player.iron = 0;
            player.silicon = 0;
            player.sulfur = 0;
            player.goldLevel = 0;
            player.shardsLevel = 0;
            player.ironLevel = 0;
            player.siliconLevel = 0;
            player.sulfurLevel = 0;
            player.energy = 50.0;
            player.maxEnergy = 50.0;

            return;
        }
        else{
            msgBox.setText("Registration failed\nUsername already exists.");
            msgBox.exec();
            sendDisconnection();
            delay(300);
            std::exit(0);
        }
    }
    else{
        msgBox.setText("Registration failed\nConnection timed out.");
        msgBox.exec();
        sendDisconnection();
        delay(300);
        std::exit(0);
    }
}

void Client::getStats()
{
    QByteArray clientSendBUf;

}

QByteArray Client::encrypt(QString input){
    std::string original = input.toStdString();
    //*inputLength = original.size();
    std::string encrypted = "";

    std::string key = "862384";

    for (unsigned int temp = 0; temp < original.size(); temp++){
            encrypted += original[temp] ^ ((atoi(key.c_str()) + temp) + 2) % 253;
    }

    QByteArray byteArray(encrypted.c_str(), encrypted.length());
    return byteArray;
}

QByteArray Client::encrypt(char* input){
    std::string original = input;
    //*inputLength = original.size();
    std::string encrypted = "";

    std::string key = "862384";

    for (unsigned int temp = 0; temp < original.size(); temp++){
            encrypted += original[temp] ^ ((atoi(key.c_str()) + temp) + 2) % 253;
    }

    QByteArray byteArray(encrypted.c_str(), encrypted.length());
    return byteArray;
}

QString Client::decrypt(QByteArray input, unsigned int inputLength){

    std::string unencrypt = "";
    std::string key = "862384";

    for (unsigned int temp = 0; temp < inputLength; temp++){
        unencrypt += input[temp] ^ ((atoi(key.c_str()) + temp) + 2) % 253;
    }
    QString output = QString::fromStdString(unencrypt);

    return output;

}
