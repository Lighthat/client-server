#include "miningwindow.h"
#include "ui_miningwindow.h"
#include <QMessageBox>

MiningWindow::MiningWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::MiningWindow)
{
    ui->setupUi(this);
    this->setWindowFlags((
    (windowFlags() | Qt::CustomizeWindowHint)
    & ~Qt::WindowCloseButtonHint
    ));
}

MiningWindow::~MiningWindow()
{
    emit mineClosed();
    delete ui;
}

void MiningWindow::on_exitButton_clicked()
{
    close();
}

void MiningWindow::getPlayer(Player *sentPlayer, Client *sentClient){
    player = sentPlayer;
    client = sentClient;
    connect(client, SIGNAL(actionFailed(int)), this, SLOT(actionFailedSlot(int)));
    connect(client, SIGNAL(actionSuccess(int)), this, SLOT(actionSuccessSlot(int)));
    ui->mineAttLabel->setText(QString::number(player->miningattempts));
    ui->mineUpLabel->setText(QString::number(player->miningupgrades));
    ui->goldLabel->setText(QString::number(player->gold));
    ui->shardsLabel->setText(QString::number(player->shards));
    ui->ironLabel->setText(QString::number(player->iron));
    ui->siliconLabel->setText(QString::number(player->silicon));
    ui->sulfurLabel->setText(QString::number(player->sulfur));
    ui->goldLvlLabel->setText(QString::number(player->goldLevel));
    ui->shardsLvlLabel->setText(QString::number(player->shardsLevel));
    ui->ironLvlLabel->setText(QString::number(player->ironLevel));
    ui->siliconLvlLabel->setText(QString::number(player->siliconLevel));
    ui->sulfurLvlLabel->setText(QString::number(player->sulfurLevel));
    ui->resptLabel->setText(QString::number(player->resetpoints));
}

void MiningWindow::on_goldAdd_clicked()
{
    if(player->miningupgrades > 0){
        player->goldLevel++;
        player->miningupgrades--;
        ui->goldLvlLabel->setText(QString::number(player->goldLevel));
        ui->mineUpLabel->setText(QString::number(player->miningupgrades));
        client->performAction(13);
    }
    else{
        QMessageBox msgBox;
        msgBox.setText("No mine upgrades available.");
        msgBox.exec();
    }
}

void MiningWindow::on_shardsAdd_clicked()
{
    if(player->miningupgrades > 0){
        player->shardsLevel++;
        player->miningupgrades--;
        ui->shardsLvlLabel->setText(QString::number(player->shardsLevel));
        ui->mineUpLabel->setText(QString::number(player->miningupgrades));
        client->performAction(14);
    }
    else{
        QMessageBox msgBox;
        msgBox.setText("No mine upgrades available.");
        msgBox.exec();
    }
}

void MiningWindow::on_ironAdd_clicked()
{
    if(player->miningupgrades > 0){
        player->ironLevel++;
        player->miningupgrades--;
        ui->ironLvlLabel->setText(QString::number(player->ironLevel));
        ui->mineUpLabel->setText(QString::number(player->miningupgrades));
        client->performAction(15);
    }
    else{
        QMessageBox msgBox;
        msgBox.setText("No mine upgrades available.");
        msgBox.exec();
    }
}

void MiningWindow::on_siliconAdd_clicked()
{
    if(player->miningupgrades > 0){
        player->siliconLevel++;
        player->miningupgrades--;
        ui->siliconLvlLabel->setText(QString::number(player->siliconLevel));
        ui->mineUpLabel->setText(QString::number(player->miningupgrades));
        client->performAction(16);
    }
    else{
        QMessageBox msgBox;
        msgBox.setText("No mine upgrades available.");
        msgBox.exec();
    }
}

void MiningWindow::on_sulfurAdd_clicked()
{
    if(player->miningupgrades > 0){
        player->sulfurLevel++;
        player->miningupgrades--;
        ui->sulfurLvlLabel->setText(QString::number(player->sulfurLevel));
        ui->mineUpLabel->setText(QString::number(player->miningupgrades));
        client->performAction(17);
    }
    else{
        QMessageBox msgBox;
        msgBox.setText("No mine upgrades available.");
        msgBox.exec();
    }
}

void MiningWindow::on_goldSub_clicked()
{
    if(player->resetpoints > 0 && player->goldLevel > 0){
        player->goldLevel--;
        player->resetpoints--;
        player->miningupgrades++;
        ui->goldLvlLabel->setText(QString::number(player->goldLevel));
        ui->mineUpLabel->setText(QString::number(player->miningupgrades));
        ui->resptLabel->setText(QString::number(player->resetpoints));
        client->performAction(18);
    }
    else if(!player->resetpoints > 0){
        QMessageBox msgBox;
        msgBox.setText("No reset points available.");
        msgBox.exec();
    }
    else{
        QMessageBox msgBox;
        msgBox.setText("Gold mine isn't high enough level");
        msgBox.exec();
    }
}

void MiningWindow::on_shardsSub_clicked()
{
    if(player->resetpoints > 0 && player->shardsLevel > 0){
        player->shardsLevel--;
        player->resetpoints--;
        player->miningupgrades++;
        ui->shardsLvlLabel->setText(QString::number(player->shardsLevel));
        ui->mineUpLabel->setText(QString::number(player->miningupgrades));
        ui->resptLabel->setText(QString::number(player->resetpoints));
        client->performAction(19);
    }
    else if(!player->resetpoints > 0){
        QMessageBox msgBox;
        msgBox.setText("No reset points available.");
        msgBox.exec();
    }
    else{
        QMessageBox msgBox;
        msgBox.setText("Shard mine isn't high enough level");
        msgBox.exec();
    }
}

void MiningWindow::on_ironSub_clicked()
{
    if(player->resetpoints > 0 && player->ironLevel > 0){
        player->ironLevel--;
        player->resetpoints--;
        player->miningupgrades++;
        ui->ironLvlLabel->setText(QString::number(player->ironLevel));
        ui->mineUpLabel->setText(QString::number(player->miningupgrades));
        ui->resptLabel->setText(QString::number(player->resetpoints));
        client->performAction(20);
    }
    else if(!player->resetpoints > 0){
        QMessageBox msgBox;
        msgBox.setText("No reset points available.");
        msgBox.exec();
    }
    else{
        QMessageBox msgBox;
        msgBox.setText("Iron mine isn't high enough level");
        msgBox.exec();
    }
}

void MiningWindow::on_siliconSub_clicked()
{
    if(player->resetpoints > 0 && player->siliconLevel > 0){
        player->siliconLevel--;
        player->resetpoints--;
        player->miningupgrades++;
        ui->siliconLvlLabel->setText(QString::number(player->siliconLevel));
        ui->mineUpLabel->setText(QString::number(player->miningupgrades));
        ui->resptLabel->setText(QString::number(player->resetpoints));
        client->performAction(21);
    }
    else if(!player->resetpoints > 0){
        QMessageBox msgBox;
        msgBox.setText("No reset points available.");
        msgBox.exec();
    }
    else{
        QMessageBox msgBox;
        msgBox.setText("Silicon mine isn't high enough level");
        msgBox.exec();
    }
}

void MiningWindow::on_sulfurSub_clicked()
{
    if(player->resetpoints > 0 && player->sulfurLevel > 0){
        player->sulfurLevel--;
        player->resetpoints--;
        player->miningupgrades++;
        ui->sulfurLvlLabel->setText(QString::number(player->sulfurLevel));
        ui->mineUpLabel->setText(QString::number(player->miningupgrades));
        ui->resptLabel->setText(QString::number(player->resetpoints));
        client->performAction(22);
    }
    else if(!player->resetpoints > 0){
        QMessageBox msgBox;
        msgBox.setText("No reset points available.");
        msgBox.exec();
    }
    else{
        QMessageBox msgBox;
        msgBox.setText("Sulfur mine isn't high enough level");
        msgBox.exec();
    }
}

void MiningWindow::on_mineButton_clicked()
{
    bool ok = true;
    int attemptsToUse = 0;
    oldPlayer = *player;

    attemptsToUse = QInputDialog::getInt(NULL, "Mine", "Mining Attempts to use:",
                                         player->miningattempts, 0, player->miningattempts, 1, &ok);
    if(ok)
        client->performAction(23, QString::number(attemptsToUse));
}

void MiningWindow::actionFailedSlot(int ID){
    if (ID == 7){
        QMessageBox msgBox;
        msgBox.setText("Mining failed - invalid number of attempts used.\nStop packet editing!");
        msgBox.exec();
    }
    else{
        QMessageBox msgBox;
        msgBox.setText("Odd error - expected action ID 7, got ID " + ID);
        msgBox.exec();
    }
}

void MiningWindow::actionSuccessSlot(int ID){

    if (ID == 6){
        int goldMined = player->gold - oldPlayer.gold,
            shardsMined = player->shards - oldPlayer.shards,
            ironMined = player->iron - oldPlayer.iron,
            siliconMined = player->silicon - oldPlayer.silicon,
            sulfurMined = player->sulfur - oldPlayer.sulfur;
        ui->mineAttLabel->setText(QString::number(player->miningattempts));
        ui->goldLabel->setText(QString::number(player->gold));
        ui->shardsLabel->setText(QString::number(player->shards));
        ui->ironLabel->setText(QString::number(player->iron));
        ui->siliconLabel->setText(QString::number(player->silicon));
        ui->sulfurLabel->setText(QString::number(player->sulfur));
        QMessageBox msgBox;
        msgBox.setText("Mining Successful!\nGold Mined: " + QString::number(goldMined) +
                       "\nShards Mined: " + QString::number(shardsMined) +
                       "\nIron Mined: " + QString::number(ironMined) +
                       "\nSilicon Mined: " + QString::number(siliconMined) +
                       "\nSulfur Mined: " + QString::number(sulfurMined));
        msgBox.exec();
    }
    else if (ID == 8){
        ui->mineAttLabel->setText(QString::number(player->miningattempts));
    }
    else{}

}

