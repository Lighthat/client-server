#ifndef CLIENT_H
#define CLIENT_H

#define PORT_NUMBER 7609

#include <QtNetwork>
#include <QObject>
#include <QString>
#include <QTcpSocket>
#include <QStringList>
#include <QInputDialog>
#include <QTime>
#include "player.h"

class Client: public QObject
{
Q_OBJECT

    typedef struct {
        int activityID;
        int stringCount;
        QList<int> stringLength;
        QStringList stringData;
    }dataToSend;



public:
  Client(QObject* parent = 0);
  ~Client();
  void start(QString address, quint16 port);\
  QByteArray encrypt(QString input);
  QByteArray encrypt(char* input);
  QString decrypt(QByteArray input, unsigned int inputLength);
  bool login(QString username, QString password);
  bool getLoginInfo();
  bool initialRun();
  bool getRegistrationInfo();
  void registration(QString username, QString password);
  void sendDisconnection();
  void delay(int msToWait);
  void readData(std::string serverReceiveBuf);
  void getStats();
  void performAction(int activityID, QStringList stringData, int stringCount);
  void performAction(int activityID, QString stringData);
  void performAction(int activityID);
  void featurePlaceholder();

  bool validated;
  QTcpSocket client;
  Player player;
  int timeoutTimer;
signals:
  void hasData(QByteArray data);
  void actionFailed(int ID);
  void actionSuccess(int ID);
  //void doneReading(char*  dataBuffer);
public slots:
  void startTransfer();
  void acceptResponse(QByteArray data);
  void getHeader();


private:


};


#endif //CLIENT_H
