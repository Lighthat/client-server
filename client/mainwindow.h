#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "client.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    Client client;

private slots:
    void update();
    void on_exitButton_clicked();
    void on_strAdd_clicked();
    void on_guiAdd_clicked();
    void on_intAdd_clicked();
    void on_resAdd_clicked();
    void on_strSub_clicked();
    void on_guiSub_clicked();
    void on_intSub_clicked();
    void on_resSub_clicked();
    void on_healButton_clicked();
    void on_marketButton_clicked();
    void on_inventoryButton_clicked();
    void on_storeButton_clicked();
    void on_mineButton_clicked();
    void on_labButton_clicked();
    void on_craftingButton_clicked();
    void on_window_close();

    void actionSuccessSlot(int);
    void timeoutTick();

    void on_pushButton_clicked();

    void on_adventureButton_clicked();

private:
    Ui::MainWindow *ui;


};

#endif // MAINWINDOW_H
