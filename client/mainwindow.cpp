#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QInputDialog>
#include <iostream>
#include "inventorywindow.h"
#include "miningwindow.h"
#include "labwindow.h"
#include "adventurewindow.h"
#include <time.h>
#include <QTimer>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    client.start("127.0.0.1", PORT_NUMBER);

    if(!client.client.waitForConnected(15000)){
        QMessageBox msgBox;
        msgBox.setText("Could not connect to server.\n" + client.client.error());
        msgBox.exec();
        std::exit(0);
    }
    this->setWindowFlags((
    (windowFlags() | Qt::CustomizeWindowHint)
    & ~Qt::WindowCloseButtonHint
    ));



    if(client.initialRun()){
        srand( time( NULL ));
        client.validated = true;
        ui->usernameLabel->setText(client.player.username.toLower());
        ui->strValLabel->setText(QString::number(client.player.strength));
        ui->guiValLabel->setText(QString::number(client.player.guile));
        ui->intValLabel->setText(QString::number(client.player.intelligence));
        ui->resValLabel->setText(QString::number(client.player.resilience));
        ui->stptLabel->setText(QString::number(client.player.statpoints));
        ui->resptLabel->setText(QString::number(client.player.resetpoints));
        ui->mineAttLabel->setText(QString::number(client.player.miningattempts));
        ui->mineUpLabel->setText(QString::number(client.player.miningupgrades));
        ui->levelLabel->setText(QString::number(client.player.level));
        ui->experienceLabel->setText(QString::number(client.player.experience));
        QString hpDisplay;
        hpDisplay.append(QString::number(client.player.currentHP));
        hpDisplay.append(" / ");
        hpDisplay.append(QString::number(client.player.maxHP));
        ui->hpValLabel->setText(hpDisplay);
        QString manaDisplay;
        manaDisplay.append(QString::number(client.player.currentMana));
        manaDisplay.append(" / ");
        manaDisplay.append(QString::number(client.player.maxMana));
        ui->manaValLabel->setText(manaDisplay);
        QString energyDisplay;
        energyDisplay.append(QString::number(client.player.energy));
        energyDisplay.append(" / ");
        energyDisplay.append(QString::number(client.player.maxEnergy));
        ui->energyValLabel->setText(energyDisplay);

        QTimer *timer = new QTimer(this);
        connect(timer, SIGNAL(timeout()), this, SLOT(update()));
        timer->start(1000);
        connect(&client, SIGNAL(actionSuccess(int)), this, SLOT(actionSuccessSlot(int)));

        QTimer *timeoutTicker = new QTimer(this);
        connect(timeoutTicker, SIGNAL(timeout()), this, SLOT(timeoutTick()));
        timeoutTicker->start(1000);
    }
    else
        client.validated = false;
}

MainWindow::~MainWindow()
{
    delete ui;
    delete &client;
}

void MainWindow::update(){
    client.performAction(24);
}

void MainWindow::timeoutTick(){
    client.timeoutTimer++;
    if(client.timeoutTimer > 30){
        QMessageBox msgBox;
        msgBox.setText("Timeout Error - cannot communicate with server.");
        msgBox.exec();
        std::exit(0);
    }
}

void MainWindow::on_exitButton_clicked()
{
    client.sendDisconnection();
    client.delay(300);
    std::exit(0);
}

void MainWindow::on_strAdd_clicked()
{
    if(client.player.statpoints != 0){
        client.player.strength++;
        client.player.statpoints--;
        client.performAction(5);
        ui->strValLabel->setText(QString::number(client.player.strength));
        ui->stptLabel->setText(QString::number(client.player.statpoints));
    }
    else{
        QMessageBox msgBox;
        msgBox.setText("No statpoints available.");
        msgBox.exec();
    }
}

void MainWindow::on_guiAdd_clicked()
{
    if(client.player.statpoints != 0){
        client.player.guile++;
        client.player.statpoints--;
        client.performAction(6);
        ui->guiValLabel->setText(QString::number(client.player.guile));
        ui->stptLabel->setText(QString::number(client.player.statpoints));
    }
    else{
        QMessageBox msgBox;
        msgBox.setText("No statpoints available.");
        msgBox.exec();
    }
}

void MainWindow::on_intAdd_clicked()
{
    if(client.player.statpoints != 0){
        client.player.intelligence++;
        client.player.maxMana += 3;
        client.player.currentMana += 3;
        client.player.statpoints--;
        client.performAction(7);
        QString manaDisplay;
        manaDisplay.append(QString::number(client.player.currentMana));
        manaDisplay.append(" / ");
        manaDisplay.append(QString::number(client.player.maxMana));
        ui->manaValLabel->setText(manaDisplay);
        ui->intValLabel->setText(QString::number(client.player.intelligence));
        ui->stptLabel->setText(QString::number(client.player.statpoints));
    }
    else{
        QMessageBox msgBox;
        msgBox.setText("No statpoints available.");
        msgBox.exec();
    }
}

void MainWindow::on_resAdd_clicked()
{
    if(client.player.statpoints != 0){
        client.player.resilience++;
        client.player.maxHP += 7;
        client.player.currentHP += 7;
        client.player.statpoints--;
        client.performAction(8);
        QString hpDisplay;
        hpDisplay.append(QString::number(client.player.currentHP));
        hpDisplay.append(" / ");
        hpDisplay.append(QString::number(client.player.maxHP));
        ui->hpValLabel->setText(hpDisplay);
        ui->resValLabel->setText(QString::number(client.player.resilience));
        ui->stptLabel->setText(QString::number(client.player.statpoints));
    }
    else{
        QMessageBox msgBox;
        msgBox.setText("No statpoints available.");
        msgBox.exec();
    }
}

void MainWindow::on_strSub_clicked()
{
    if(client.player.resetpoints > 0 && client.player.strength > 0){
        client.player.strength--;
        client.player.resetpoints--;
        client.player.statpoints++;
        client.performAction(9);
        ui->strValLabel->setText(QString::number(client.player.strength));
        ui->stptLabel->setText(QString::number(client.player.statpoints));
        ui->resptLabel->setText(QString::number(client.player.resetpoints));
    }
    else if(!client.player.resetpoints > 0){
        QMessageBox msgBox;
        msgBox.setText("No reset points available.");
        msgBox.exec();
    }
    else{
        QMessageBox msgBox;
        msgBox.setText("You cannot subtract strength when you have none.");
        msgBox.exec();
    }
}

void MainWindow::on_guiSub_clicked()
{
    if(client.player.resetpoints > 0 && client.player.guile > 0){
        client.player.guile--;
        client.player.resetpoints--;
        client.player.statpoints++;
        client.performAction(10);
        ui->guiValLabel->setText(QString::number(client.player.guile));
        ui->stptLabel->setText(QString::number(client.player.statpoints));
        ui->resptLabel->setText(QString::number(client.player.resetpoints));
    }
    else if(!client.player.resetpoints > 0){
        QMessageBox msgBox;
        msgBox.setText("No reset points available.");
        msgBox.exec();
    }
    else{
        QMessageBox msgBox;
        msgBox.setText("You cannot subtract guile when you have none.");
        msgBox.exec();
    }
}

void MainWindow::on_intSub_clicked()
{
    if(client.player.resetpoints > 0 && client.player.intelligence > 0){
        client.player.intelligence--;
        client.player.resetpoints--;
        if(client.player.maxMana > 3)
            client.player.maxMana -= 3;
        else
            client.player.maxMana = 0;
        if(client.player.currentMana > 3)
            client.player.currentMana -= 3;
        else
            client.player.currentMana = 0;
        client.player.statpoints++;
        client.performAction(11);
        QString manaDisplay;
        manaDisplay.append(QString::number(client.player.currentMana));
        manaDisplay.append(" / ");
        manaDisplay.append(QString::number(client.player.maxMana));
        ui->manaValLabel->setText(manaDisplay);
        ui->intValLabel->setText(QString::number(client.player.intelligence));
        ui->stptLabel->setText(QString::number(client.player.statpoints));
        ui->resptLabel->setText(QString::number(client.player.resetpoints));
    }
    else if(!client.player.resetpoints > 0){
        QMessageBox msgBox;
        msgBox.setText("No reset points available.");
        msgBox.exec();
    }
    else{
        QMessageBox msgBox;
        msgBox.setText("You cannot subtract intelligence when you have none, idiot.");
        msgBox.exec();
    }
}

void MainWindow::on_resSub_clicked()
{
    if(client.player.resetpoints > 0 && client.player.resilience > 0){
        client.player.resilience--;
        client.player.resetpoints--;
        if(client.player.maxHP > 7)
            client.player.maxHP -= 7;
        else
            client.player.maxHP = 1;
        if(client.player.currentHP > 7)
            client.player.currentHP -= 7;
        else if(client.player.currentHP < 7 && client.player.currentHP != 0)
            client.player.currentHP = 1;
        else
            client.player.currentHP = 0;
        client.player.statpoints++;
        client.performAction(12);
        QString hpDisplay;
        hpDisplay.append(QString::number(client.player.currentHP));
        hpDisplay.append(" / ");
        hpDisplay.append(QString::number(client.player.maxHP));
        ui->hpValLabel->setText(hpDisplay);
        ui->resValLabel->setText(QString::number(client.player.resilience));
        ui->stptLabel->setText(QString::number(client.player.statpoints));
        ui->resptLabel->setText(QString::number(client.player.resetpoints));
    }
    else if(!client.player.resetpoints > 0){
        QMessageBox msgBox;
        msgBox.setText("No reset points available.");
        msgBox.exec();
    }
    else{
        QMessageBox msgBox;
        msgBox.setText("You cannot subtract resilience when you have none.");
        msgBox.exec();
    }
}

void MainWindow::on_healButton_clicked()
{
    client.featurePlaceholder();
}

void MainWindow::on_marketButton_clicked()
{
    client.featurePlaceholder();
}

void MainWindow::on_inventoryButton_clicked()
{
    InventoryWindow *w = new InventoryWindow;
    w->setAttribute(Qt::WA_DeleteOnClose);
    this->setEnabled(false);
    connect(w, SIGNAL(inventoryClosed()), this, SLOT(on_window_close()));
    w->getPlayer(client.player);
    w->show();
}

void MainWindow::on_storeButton_clicked()
{
    client.featurePlaceholder();
}

void MainWindow::on_mineButton_clicked()
{
    MiningWindow *w = new MiningWindow;
    w->setAttribute(Qt::WA_DeleteOnClose);
    this->setEnabled(false);
    connect(w, SIGNAL(mineClosed()), this, SLOT(on_window_close()));
    w->getPlayer(&client.player, &client);
    w->show();
}
void MainWindow::on_labButton_clicked()
{
   // LabWindow *w = new LabWindow;
   // w->setAttribute(Qt::WA_DeleteOnClose);
   // this->setEnabled(false);
   // connect(w, SIGNAL(labClosed()), this, SLOT(on_window_close()));
   // w->getPlayer(&client.player, &client);
   // w->show();
     client.featurePlaceholder();
}

void MainWindow::on_craftingButton_clicked()
{
    client.featurePlaceholder();
}

void MainWindow::on_window_close(){
    this->setEnabled(true);
}

void MainWindow::actionSuccessSlot(int ID){
    if(ID == 8){
        ui->mineAttLabel->setText(QString::number(client.player.miningattempts));
        QString energyDisplay;
        energyDisplay.append(QString::number(client.player.energy));
        energyDisplay.append(" / ");
        energyDisplay.append(QString::number(client.player.maxEnergy));
        ui->energyValLabel->setText(energyDisplay);
    }
    else{}
}

void MainWindow::on_pushButton_clicked()
{
    client.performAction(28);
}

void MainWindow::on_adventureButton_clicked()
{
    AdventureWindow *w = new AdventureWindow;
    w->setAttribute(Qt::WA_DeleteOnClose);
    this->setEnabled(false);
    connect(w, SIGNAL(adventureClosed()), this, SLOT(on_window_close()));
    w->getPlayer(&client.player, &client);
    w->show();
}
