#ifndef MININGWINDOW_H
#define MININGWINDOW_H

#include <QWidget>
#include "player.h"
#include "client.h"

namespace Ui {
class MiningWindow;
}

class MiningWindow : public QWidget
{
    Q_OBJECT

public:
    explicit MiningWindow(QWidget *parent = 0);
    ~MiningWindow();
    void getPlayer(Player*, Client*);

signals:
    void mineClosed();

private slots:
    void actionFailedSlot(int);
    void actionSuccessSlot(int);
    void on_exitButton_clicked();
    void on_goldAdd_clicked();
    void on_goldSub_clicked();
    void on_shardsAdd_clicked();
    void on_ironAdd_clicked();
    void on_siliconAdd_clicked();
    void on_sulfurAdd_clicked();
    void on_shardsSub_clicked();
    void on_ironSub_clicked();
    void on_siliconSub_clicked();
    void on_sulfurSub_clicked();
    void on_mineButton_clicked();

private:
    Ui::MiningWindow *ui;
    Player *player;
    Client *client;
    Player oldPlayer;
};

#endif // MININGWINDOW_H
