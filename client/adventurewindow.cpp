#include "adventurewindow.h"
#include "ui_adventurewindow.h"
#include <QMessageBox>

AdventureWindow::AdventureWindow(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AdventureWindow)
{
    ui->setupUi(this);
    this->setWindowFlags((
    (windowFlags() | Qt::CustomizeWindowHint)
    & ~Qt::WindowCloseButtonHint
    ));
}

AdventureWindow::~AdventureWindow()
{
    emit adventureClosed();
    delete ui;
}

void AdventureWindow::getPlayer(Player *sentPlayer, Client *sentClient){
    player = sentPlayer;
    client = sentClient;
    connect(client, SIGNAL(actionFailed(int)), this, SLOT(actionFailedSlot(int)));
    connect(client, SIGNAL(actionSuccess(int)), this, SLOT(actionSuccessSlot(int)));

    //Init actions - Visibility
    ui->optionOne->setEnabled(false);
    ui->optionTwo->setEnabled(false);
    ui->optionThree->setEnabled(false);
    ui->optionFour->setEnabled(false);
    ui->combatBox->setEnabled(false);

    //Init actions - Stat Display
    ui->usernameLabel->setText(player->username.toLower());
    ui->strValLabel->setText(QString::number(player->strength));
    ui->guiValLabel->setText(QString::number(player->guile));
    ui->intValLabel->setText(QString::number(player->intelligence));
    ui->resValLabel->setText(QString::number(player->resilience));
    ui->stptLabel->setText(QString::number(player->statpoints));
    ui->resptLabel->setText(QString::number(player->resetpoints));
    ui->mineAttLabel->setText(QString::number(player->miningattempts));
    ui->mineUpLabel->setText(QString::number(player->miningupgrades));
    ui->levelLabel->setText(QString::number(player->level));
    ui->experienceLabel->setText(QString::number(player->experience));
    QString hpDisplay;
    hpDisplay.append(QString::number(player->currentHP));
    hpDisplay.append(" / ");
    hpDisplay.append(QString::number(player->maxHP));
    ui->hpValLabel->setText(hpDisplay);
    QString manaDisplay;
    manaDisplay.append(QString::number(player->currentMana));
    manaDisplay.append(" / ");
    manaDisplay.append(QString::number(player->maxMana));
    ui->manaValLabel->setText(manaDisplay);
    QString energyDisplay;
    energyDisplay.append(QString::number(player->energy));
    energyDisplay.append(" / ");
    energyDisplay.append(QString::number(player->maxEnergy));
    ui->energyValLabel->setText(energyDisplay);

    //Init Actions - Player Placement
    x = -100;
    y = -100;
    QString coordinateDisplay;
    coordinateDisplay.append(QString::number(x));
    coordinateDisplay.append("E , ");
    coordinateDisplay.append(QString::number(y));
    coordinateDisplay.append("N");
    ui->coordinatesLabel->setText(coordinateDisplay);
}

void AdventureWindow::actionFailedSlot(int ID){

    //this slot handles things that come from the server.
    //since we press buttons here and need to work through the client.h file,
    //we need these slots to connect the signals emitted by the client
    //so the changes can be displayed on the widget window.
    if(ID == 0){
        this->setEnabled(true);
    }
    else if(ID == 1){
        this->setEnabled(true);
    }
}

void AdventureWindow::actionSuccessSlot(int ID){

    //this slot handles things that come from the server.
    //since we press buttons here and need to work through the client.h file,
    //we need these slots to connect the signals emitted by the client
    //so the changes can be displayed on the widget window.
    if (ID == 8){
        ui->mineAttLabel->setText(QString::number(player->miningattempts));
        QString energyDisplay;
        energyDisplay.append(QString::number(player->energy));
        energyDisplay.append(" / ");
        energyDisplay.append(QString::number(player->maxEnergy));
        ui->energyValLabel->setText(energyDisplay);
    }


}

void AdventureWindow::on_exitButton_clicked()
{
    close();
}

void AdventureWindow::on_south_clicked()
{
    if(y <= -100){
        y = -100;
        QMessageBox msgBox;
        msgBox.setText("You're already at the south edge of the zone.");
        msgBox.exec();
    }
    else{
        if(player->energy >= 0.5 && player->currentHP > 0){
            player->energy -= 0.5;
            client->performAction(26);
            --y;
            QString coordinateDisplay;
            coordinateDisplay.append(QString::number(x));
            coordinateDisplay.append("E , ");
            coordinateDisplay.append(QString::number(y));
            coordinateDisplay.append("N");
            ui->coordinatesLabel->setText(coordinateDisplay);
            QString energyDisplay;
            energyDisplay.append(QString::number(player->energy));
            energyDisplay.append(" / ");
            energyDisplay.append(QString::number(player->maxEnergy));
            ui->energyValLabel->setText(energyDisplay);
            //random encounter code
        }
        else if(player->energy < 0.5){
            QMessageBox msgBox;
            msgBox.setText("You don't have the energy to continue exploring.");
            msgBox.exec();
        }
        else{
            QMessageBox msgBox;
            msgBox.setText("You can't go on an adventure when you're dead.");
            msgBox.exec();
        }
    }
}

void AdventureWindow::on_west_clicked()
{
    if(x <= -100){
        x = -100;
        QMessageBox msgBox;
        msgBox.setText("You're already at the west edge of the zone.");
        msgBox.exec();
    }
    else{
        if(player->energy >= 0.5 && player->currentHP > 0){
            player->energy -= 0.5;
            client->performAction(26);
            --x;
            QString coordinateDisplay;
            coordinateDisplay.append(QString::number(x));
            coordinateDisplay.append("E , ");
            coordinateDisplay.append(QString::number(y));
            coordinateDisplay.append("N");
            ui->coordinatesLabel->setText(coordinateDisplay);
            QString energyDisplay;
            energyDisplay.append(QString::number(player->energy));
            energyDisplay.append(" / ");
            energyDisplay.append(QString::number(player->maxEnergy));
            ui->energyValLabel->setText(energyDisplay);
            //random encounter code
        }
        else if(player->energy < 0.5){
            QMessageBox msgBox;
            msgBox.setText("You don't have the energy to continue exploring.");
            msgBox.exec();
        }
        else{
            QMessageBox msgBox;
            msgBox.setText("You can't go on an adventure when you're dead.");
            msgBox.exec();
        }
    }
}

void AdventureWindow::on_north_clicked()
{
    if(y >= 100){
        y = 100;
        QMessageBox msgBox;
        msgBox.setText("You're already at the north edge of the zone.");
        msgBox.exec();
    }
    else{
        if(player->energy >= 0.5 && player->currentHP > 0){
            player->energy -= 0.5;
            client->performAction(26);
            ++y;
            QString coordinateDisplay;
            coordinateDisplay.append(QString::number(x));
            coordinateDisplay.append("E , ");
            coordinateDisplay.append(QString::number(y));
            coordinateDisplay.append("N");
            ui->coordinatesLabel->setText(coordinateDisplay);
            QString energyDisplay;
            energyDisplay.append(QString::number(player->energy));
            energyDisplay.append(" / ");
            energyDisplay.append(QString::number(player->maxEnergy));
            ui->energyValLabel->setText(energyDisplay);
            //random encounter code
        }
        else if(player->energy < 0.5){
            QMessageBox msgBox;
            msgBox.setText("You don't have the energy to continue exploring.");
            msgBox.exec();
        }
        else{
            QMessageBox msgBox;
            msgBox.setText("You can't go on an adventure when you're dead.");
            msgBox.exec();
        }
    }
}

void AdventureWindow::on_east_clicked()
{
    this->setEnabled(false);
    if(x >= 100){
        x = 100;
        QMessageBox msgBox;
        msgBox.setText("You're already at the east edge of the zone.");
        msgBox.exec();
    }
    else
        client->performAction(26);
    //all adventure calculations will be handled on the server to prevent tampering.
}
