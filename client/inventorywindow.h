#ifndef INVENTORYWINDOW_H
#define INVENTORYWINDOW_H

#include <QWidget>
#include "player.h"

namespace Ui {
class InventoryWindow;
}

class InventoryWindow : public QWidget
{
    Q_OBJECT

public:
    explicit InventoryWindow(QWidget *parent = 0);
    ~InventoryWindow();
    void getPlayer(Player player);

signals:
    void inventoryClosed();

private slots:
    void on_exitInventory_clicked();

private:
    Ui::InventoryWindow *ui;
    Player player;
};

#endif // INVENTORYWINDOW_H
