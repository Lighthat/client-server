#-------------------------------------------------
#
# Project created by QtCreator 2014-07-08T12:01:14
#
#-------------------------------------------------

QT       += core gui sql network


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = client
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    client.cpp \
    inventorywindow.cpp \
    miningwindow.cpp \
    labwindow.cpp \
    adventurewindow.cpp

HEADERS  += mainwindow.h \
    client.h \
    inventorywindow.h \
    player.h \
    miningwindow.h \
    labwindow.h \
    adventurewindow.h

FORMS    += mainwindow.ui \
    inventorywindow.ui \
    miningwindow.ui \
    labwindow.ui \
    adventurewindow.ui
