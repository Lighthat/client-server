#ifndef PLAYER_H
#define PLAYER_H

typedef struct {
    QString username;
    int level;
    int experience;
    int strength;
    int guile;
    int intelligence;
    int resilience;
    int statpoints;
    int resetpoints;
    int miningattempts;
    int miningupgrades;
    int money;
    int maxHP;
    int currentHP;
    int maxMana;
    int currentMana;
    int gold;
    int shards;
    int iron;
    int silicon;
    int sulfur;
    int goldLevel;
    int shardsLevel;
    int ironLevel;
    int siliconLevel;
    int sulfurLevel;
    double energy;
    double maxEnergy;
}Player;

#endif // PLAYER_H
