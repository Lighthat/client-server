#ifndef ADVENTUREWINDOW_H
#define ADVENTUREWINDOW_H

#include <QWidget>
#include "player.h"
#include "client.h"

namespace Ui {
class AdventureWindow;
}

class AdventureWindow : public QWidget
{
    Q_OBJECT

public:
    explicit AdventureWindow(QWidget *parent = 0);
    ~AdventureWindow();
    void getPlayer(Player*, Client*);

signals:
    void adventureClosed();

private slots:
    void actionFailedSlot(int);
    void actionSuccessSlot(int);

    void on_exitButton_clicked();

    void on_south_clicked();

    void on_west_clicked();

    void on_north_clicked();

    void on_east_clicked();

private:
    Ui::AdventureWindow *ui;
    Player *player;
    Client *client;
    int x, y; //coordinates
    QStringList eventDisplay, actionDisplay;
};

#endif // ADVENTUREWINDOW_H
