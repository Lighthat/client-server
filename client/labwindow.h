#ifndef LABWINDOW_H
#define LABWINDOW_H

#include <QWidget>

namespace Ui {
class LabWindow;
}

class LabWindow : public QWidget
{
    Q_OBJECT

public:
    explicit LabWindow(QWidget *parent = 0);
    ~LabWindow();

private:
    Ui::LabWindow *ui;
};

#endif // LABWINDOW_H
