#ifndef SERVERTESTSUITE_H
#define SERVERTESTSUITE_H

#include "server.h"

class ServerTestSuite
{
public:
    ServerTestSuite();
    ~ServerTestSuite();

    /** ****************************************************************************************** **
     * This class serves as the unit test suite for the entire server.                              *
     * Each function that exists in the server has a corresponding unit test.                       *
     * They will check for all exceptions and return true or false as the test result.              *
     * Even if a test fails, it will continue testing all cases and return a report to the user.    *
     * This report will outline what cases were successfully handled and which were not.            *
     * It will also return some statistics, allowing one to determine how well the code is written. *
     *                                                                                              *
     * For many cases, a username is required to perform the test.                                  *
     * We want to be sure that the test cases are accurately representing the functions they test.  *
     * To do this, we have an actual user in the database that will be used for all testing.        *
     * This test username is "a poor test dummy"                                                    *
     ** ****************************************************************************************** **/

    bool calculateAdventureEvent_t();
    bool authenticateUser_t(Server::receivedData data);
    bool registerUser_t(Server::receivedData data);
    bool verifyUserHasHealth_t(int threshold);
    bool verifyUserHasEnergy_t(int threshold);
    bool verifyUserHasMana_t(int threshold);
    bool setPlayerDead_t();
    bool modifyStrength_t(char modification);
    bool modifyGuile_t(char modification);
    bool modifyIntelligence_t(char modification);
    bool modifyResilience_t(char modification);
    bool calculateMiningResults_t(Server::receivedData data);
    bool satisfyClientUpdateRequest_t();
    bool modifyGoldLevel_t(char modification);
    bool modifyShardsLevel_t(char modification);
    bool modifyIronLevel_t(char modification);
    bool modifySiliconLevel_t(char modification);
    bool modifySulfurLevel_t(char modification);
private:
    Server server_t;
};

#endif // SERVERTESTSUITE_H
