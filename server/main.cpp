#include "server.h"
#include <QCoreApplication>

int main(int argc, char** argv)
{
  QSqlDatabase myDB;

  QCoreApplication app(argc, argv);
  Server server;
  myDB = QSqlDatabase::addDatabase("QSQLITE");
  myDB.setDatabaseName(PATH_TO_DB);
  myDB.open();
  QSqlQuery qry;
  qDebug() << "[" << server.currentTime() << "] " << "Connecting to database...";
  server.createTable();
  qDebug() << "[" << server.currentTime() << "] " << "Connection to database established.";
  qDebug() << "[" << server.currentTime() << "] " << "Refreshing clients...";
  server.delay(200);
  qry.prepare("UPDATE players SET loggedIn = 0");
  qry.exec();
  qDebug() << "[" << server.currentTime() << "] " << "Refresh done.";
  return app.exec();
}
