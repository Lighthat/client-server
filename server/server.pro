#-------------------------------------------------
#
# Project created by QtCreator 2014-07-08T12:01:42
#
#-------------------------------------------------

QT       += core
QT       += network
QT       += sql
QT       -= gui

TARGET = server
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    server.cpp \
    servertestsuite.cpp

HEADERS += \
    server.h \
    servertestsuite.h
