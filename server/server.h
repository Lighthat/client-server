#ifndef SERVER_H
#define SERVER_H

#define PATH_TO_DB "GameDB"

#include <QtNetwork>
#include <QObject>
#include <QTcpServer>
#include <QtSql>
#include <fstream>
#include <QTcpSocket>
#include <QTimer>

class Server: public QObject
{
Q_OBJECT

    typedef struct {
        QString username;
        int level;
        int experience;
        int strength;
        int guile;
        int intelligence;
        int resilience;
        int statpoints;
        int resetpoints;
        int miningattempts;
        int miningupgrades;
        int money;
        int maxHP;
        int currentHP;
        int maxMana;
        int currentMana;
        int gold;
        int shards;
        int iron;
        int silicon;
        int sulfur;
        int goldLevel;
        int shardsLevel;
        int ironLevel;
        int siliconLevel;
        int sulfurLevel;
        double energy;
        double maxEnergy;
    }Player;

public:

    typedef struct {
        int activityID;
        int stringCount;
        QList<int> stringLength;
        QStringList stringData;
    }receivedData;

  Server(QObject * parent = 0);
  ~Server();
  QString currentTime();
  void handleConnection(receivedData data, QTcpSocket *client);
  QByteArray encrypt(QString input);
  QByteArray encrypt(char* input);
  QString decrypt(QByteArray input, unsigned int inputLength);
  void createTable();
  void delay(int msToWait);
  void respond(int activityID, QStringList stringData, int stringCount, QTcpSocket *client);
  void respond(int activityID, QString stringData, QTcpSocket *client);

  //****REFACTORED FUNCTIONS

  //General functions

  /**
   * @brief Consumes energy and generates a random adventure event.
   *
   * Each adventure will consume 0.5 energy, and once the event is calculated
   * any changes in player metrics will be recorded before the event ID is sent to the client.
   */
  void calculateAdventureEvent();

  /**
   * @brief Takes the data sent from the client and authenticates the user.
   *
   * Compares the username and password sent by the client and verifies that it's invalid,
   * as well as making sure they're not already logged in.
   * @param data A structure that contains all of the data sent by the client without the header.
   * @return True or false depending on whether the authentication was successful.
   */
  bool authenticateUser(receivedData data);

  /**
   * @brief Takes the data sent from the client and registers a new user.
   *
   * Verifies that the requested username is not too long and doesn't already exist,
   * as well as setting all default values for the user.
   * @param data A structure that contains all of the data sent by the client without the header.
   * @return True or false depending on whether the registration was successful.
   */
  bool registerUser(receivedData data);

  /**
   * @brief Ensures the provided user some value of health.
   * @param username The user we want to check
   * @param threshold Checks if the current health is greater than or equal to this threshold.
   * @return True or false depending on whether the user has health that meets or exceeds the threshold.
   */
  bool verifyUserHasHealth(QString username, int threshold);

  /**
   * @brief Ensures the provided user has some value of energy.
   * @param username The user we want to check
   * @param threshold Checks if the current energy is greater than or equal to this threshold.
   * @return True or false depending on whether the user has energy that meets or exceeds the threshold.
   */
  bool verifyUserHasEnergy(QString username, int threshold);

  /**
   * @brief Ensures the provided user has some value of mana.
   * @param username The user we want to check
   * @param threshold Checks if the current mana is greater than or equal to this threshold.
   * @return True or false depending on whether the user has mana that meets or exceeds the threshold.
   */
  bool verifyUserHasMana(QString username, int threshold);

  /**
   * @brief Sets current player's HP to 0
   * @param username The user we want to kill
   */
  void setPlayerDead(QString username);

  /**
   * @brief calculates the results of a user's mining.
   *
   * This function takes the data from the client and uses the level of their mines to calculate how much of
   * each material they're going to get, records those numbers in the database, and sends those results back to the client.
   * @param data A structure that contains all of the data sent by the client without the header.
   */
  void calculateMiningResults(receivedData data);

  /**
   * @brief Clients will automatically and periodically request an update from the server;
   *        this function will handle those requests.
   * @param username The user who requested the update.
   */
  void satisfyClientUpdateRequest(QString username);

  //Player Stats

  /**
   * @brief Alters the strength of the provided user by 1, returns an exception if the `modification` parameter is invalid.
   * @param username The user who's strength we want to modify
   * @param modification '+' if we want to increase it, '-' if we want to decrease it
   */
  void modifyStrength(QString username, char modification);

  /**
   * @brief Alters the guile of the provided user by 1, returns an exception if the 'modification' parameter is invalid.
   * @param username The user who's guile we want to modify
   * @param modification '+' if we want to increase it, '-' if we want to decrease it
   */
  void modifyGuile(QString username, char modification);

  /**
   * @brief Alters the intelligence of the provided user by 1, returns an exception if the 'modification' parameter is invalid.
   * @param username The user who's intelligence we want to modify.
   * @param modification '+' if we want to increase it, '-' if we want to decrease it
   */
  void modifyIntelligence(QString username, char modification);

  /**
   * @brief Alters the resilience of the provided user by 1, returns an exception if the 'modification' parameter is invalid.
   * @param username The user who's resilience we want to modify.
   * @param modification '+' if we want to increase it, '-' if we want to decrease it
   */
  void modifyResilience(QString username, char modification);

  //Mine Levels

  /**
   * @brief Alters the level of the provided user's gold mine by 1, returns an exception if the 'modification' parameter is invalid.
   * @param username The user who's mine we want to modify.
   * @param modification '+' if we want to increase it, '-' if we want to decrease it
   */
  void modifyGoldLevel(QString username, char modification);

  /**
   * @brief Alters the level of the provided user's shard mine by 1, returns an exception if the 'modification' parameter is invalid.
   * @param username The user who's mine we want to modify.
   * @param modification '+' if we want to increase it, '-' if we want to decrease it
   */
  void modifyShardsLevel(QString username, char modification);

  /**
   * @brief Alters the level of the provided user's iron mine by 1, returns an exception if the 'modification' parameter is invalid.
   * @param username The user who's mine we want to modify.
   * @param modification '+' if we want to increase it, '-' if we want to decrease it
   */
  void modifyIronLevel(QString username, char modification);

  /**
   * @brief Alters the level of the provided user's silicon mine by 1, returns an exception if the 'modification' parameter is invalid.
   * @param username The user who's mine we want to modify.
   * @param modification '+' if we want to increase it, '-' if we want to decrease it
   */
  void modifySiliconLevel(QString username, char modification);

  /**
   * @brief Alters the level of the provided user's sulfur mine by 1, returns an exception if the 'modification' parameter is invalid.
   * @param username The user who's mine we want to modify.
   * @param modification '+' if we want to increase it, '-' if we want to decrease it
   */
  void modifySulfurLevel(QString username, char modification);

public slots:
  /**
   * @brief update
   */
  void update();

  /**
   * @brief sendClientLifeCheck
   */
  void sendClientLifeCheck();

  /**
   * @brief acceptConnection
   */
  void acceptConnection();

  /**
   * @brief clientDisconnected
   */
  void clientDisconnected();

  /**
   * @brief acceptRequest
   */
  void acceptRequest();
private:
  QTcpServer server;
  QList<QTcpSocket *> clients;
  QList<QString> usernames;
};


#endif // SERVER_H
