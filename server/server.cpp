#define PORT_NUMBER 7609

#include "server.h"
#include <iostream>
#include <time.h>
#include <stdlib.h>
#include <stdint.h>
#include <QtEndian>
#include <cmath>
#include <iomanip>
using namespace std;

Server::Server(QObject* parent): QObject(parent)
{
    connect(&server, SIGNAL(newConnection()),
    this, SLOT(acceptConnection()));

    QTimer *updateTick = new QTimer(this);
    connect(updateTick, SIGNAL(timeout()), this, SLOT(update()));
    updateTick->start(1000);

    QTimer *lifeTick = new QTimer(this);
    connect(lifeTick, SIGNAL(timeout()), this, SLOT(sendClientLifeCheck()));
    lifeTick->start(20000);

    qDebug() << "[" << currentTime() << "] " << "Server started.";

    server.listen(QHostAddress::Any, PORT_NUMBER);
    srand( time( NULL ));

    qDebug() << "[" << currentTime() << "] " << "Server listening.";
}

Server::~Server()
{
  qDeleteAll(clients);
  server.close();
}

void Server::update(){
    QSqlQuery qry0, qry1;
    qry0.exec("SELECT COUNT(*) FROM players");
    qry0.next();
    int numPlayers = qry0.value(0).toInt();
    qry0.exec("SELECT maxEnergy, energy, id FROM players");
    for (int i = 0; i < numPlayers; i++){
        qry0.next();
        int playerID = qry0.value(2).toInt();
        if(qry0.value(1).toDouble() + 5.0 < qry0.value(0).toDouble()){
            //not energy capped, add 5 energy
            qry1.prepare("UPDATE players SET energy=energy+5.0 WHERE id=:f1");
            qry1.bindValue(":f1",playerID);
            qry1.exec();
        }
        else{
            //energy is less than 5 below (or equal to) max energy, so we set it to cap
            qry1.prepare("UPDATE players SET energy=maxEnergy WHERE id=:f1");
            qry1.bindValue(":f1",playerID);
            qry1.exec();
        }
    }
    qry0.prepare("UPDATE players SET miningattempts=miningattempts+1");
    qry0.exec();
}

void Server::sendClientLifeCheck(){
    for(int i = 0; i < clients.size(); i++){
        respond(9, "isAlive", clients[i]);
    }
}

void Server::acceptConnection()
{
        QTcpSocket *client = server.nextPendingConnection();
        connect(client, SIGNAL(disconnected()),
                this, SLOT(clientDisconnected()));
        connect(client, SIGNAL(readyRead()),
                this, SLOT(acceptRequest()));
        clients.append(client);
        usernames.append("Pending User");
}

void Server::acceptRequest(){

    QTcpSocket *client = qobject_cast<QTcpSocket *>(sender());
    if(!client)
        return;

    receivedData data;

    while(client->bytesAvailable()){
        QByteArray buffer;

        //first we get the header from the stream
        uint16_t dataSize;
        client->read((char*)&dataSize, sizeof(uint16_t));
        buffer = client->read(dataSize);

        //now we get the expected bytes for this request from the stream
        while(buffer.size() < dataSize)
        {
            client->waitForReadyRead();
            buffer.append(client->read(dataSize - buffer.size()));
        }

        QString stringBuilder;
        try{
            if(buffer.length() > 2){
                data.activityID = buffer.at(0);
                data.stringCount = buffer.at(1);
                int i,
                    j = 2, //placeholder for next slot to be used in the receive buffer
                    k = 0;
                for (i = 0; i < data.stringCount; i++){ //while there are strings whose lengths we need to document
                    data.stringLength.insert(i, buffer.at(2+i));
                    j++;
                }
                while(k < data.stringCount){ //while there are strings in the buffer to be put into the data structure
                    i = 0;
                    while(i < data.stringLength.at(k)){ //while there are characters in the buffer to be built into a string
                        stringBuilder.append(buffer.at(j));
                        j++;
                        i++;
                    }
                    //we have assembled a string from chars in the buffer
                    //now we put them into the struct
                    data.stringData << stringBuilder;
                    stringBuilder = ""; //empty string builder for next string.
                    k++;
                }
            }
            else{
                data.activityID = -1;
            }
        }
        catch(std::exception e){
            qDebug() << "[" << currentTime() << "] " << e.what() << "An undefined buffer error occurred.";
        }
    }
    handleConnection(data, client);
}
void Server::clientDisconnected()
{
    QTcpSocket *client = qobject_cast<QTcpSocket *>(sender());
    QSqlQuery qry;

        if (!client)
            return;

        qDebug() << "[" << currentTime() << "] " << "User has disconnected.";
        int index = clients.indexOf(client);
        qry.prepare("UPDATE players SET loggedIn=0 WHERE username=:f1");
        qry.bindValue(":f1",usernames[index]);
        qry.exec();
        clients.removeAll(client);
        usernames.removeAt(index);
        client->deleteLater();

        /*foreach(QTcpSocket *client, clients) {
                client->write("");
            }*/ //message to all clients, archaic
}


//FUNCTION TOO LARGE
//Cleanup:
// * Separate functions for each activity ID
// * Maybe split up some of the larger activity IDs into classes (adventure comes to mind)
void Server::handleConnection(receivedData data, QTcpSocket *client){

    QSqlQuery qry;

    if(data.activityID == -1){
        qDebug() << "[" << currentTime() << "] " << "A null buffer error occurred.";
    }

    else if(data.activityID == 0){ //player death
        QString decryptedUsername = decrypt(data.stringData.at(0).toUtf8(), data.stringData.at(0).length());
        qry.prepare("UPDATE players SET currentHP=0 WHERE username=:f0");
        qry.bindValue(":f0",decryptedUsername);
        qry.exec();
        return;
    }

    if(data.activityID == 1){ //client greeting
        qDebug() << "[" << currentTime() << "] " << decrypt(data.stringData.at(0).toUtf8(), data.stringData.at(0).length());
        return;
    }
    if(data.activityID == 2){ //user login
        QString decryptedUsername = decrypt(data.stringData.at(0).toUtf8(), data.stringData.at(0).length());
        decryptedUsername = decryptedUsername.toLower();
        qDebug() << "[" << currentTime() << "] " << "Validating user" << decryptedUsername;
        qry.prepare("SELECT password FROM players WHERE username=:f1");
        qry.bindValue(":f1", decryptedUsername);
        if(qry.exec()){
            qry.next();
            if (qry.value(0).toString() == data.stringData.at(1))
            {
                QStringList stats;
                qry.prepare("SELECT level, experience, strength, guile, intelligence, resilience, statpoints, resetpoints, miningattempts, miningupgrades, money, maxHP, currentHP, maxMana, currentMana, gold, shards, iron, silicon, sulfur, goldLevel, shardsLevel, ironLevel, siliconLevel, sulfurLevel, loggedIn, energy, maxEnergy FROM players WHERE username=:f1");
                qry.bindValue(":f1", decryptedUsername);
                if(qry.exec()){
                    qry.next();
                    if(qry.value(25).toInt() == 1){
                        qDebug() << "[" << currentTime() << "] " << "C10: Validation for user" << decryptedUsername << "failed!";
                        respond(10, "alreadylogged", client);
                        return;
                    }
                    stats << qry.value(0).toString() << qry.value(1).toString() << qry.value(2).toString() << qry.value(3).toString() << qry.value(4).toString() << qry.value(5).toString() << qry.value(6).toString();
                    stats << qry.value(7).toString() << qry.value(8).toString() << qry.value(9).toString() << qry.value(10).toString() << qry.value(11).toString() << qry.value(12).toString() << qry.value(13).toString();
                    stats << qry.value(14).toString() << qry.value(15).toString() << qry.value(16).toString() << qry.value(17).toString() << qry.value(18).toString() << qry.value(19).toString() << qry.value(20).toString();
                    stats << qry.value(21).toString() << qry.value(22).toString() << qry.value(23).toString() << qry.value(24).toString() << qry.value(25).toString() << qry.value(26).toString();
                    qry.prepare("UPDATE players SET loggedIn=1 WHERE username=:f1");
                    qry.bindValue(":f1", decryptedUsername);
                    qry.exec();
                    int index = clients.indexOf(client);
                    usernames[index] = decryptedUsername;
                    respond(3, stats, stats.length(), client);
                    qDebug() << "[" << currentTime() << "] " << "C3: Validation for user" << decryptedUsername << "succeeded.";
                    return;
                }
                return;

            }
            else
            {
                qDebug() << "[" << currentTime() << "] " << "C4: Validation for user" << decryptedUsername << "failed!";
                respond(4, "logvalfail", client);
                return;
            }
            qDebug() << "[" << currentTime() << "] " << "C4: Validation for user" << decryptedUsername << "failed!";
            respond(4, "logvalfail", client);
            return;
        }
        return;
    }
    if(data.activityID == 3){ //user registration
        QString decryptedUsername = decrypt(data.stringData.at(0).toUtf8(), data.stringData.at(0).length());
        decryptedUsername = decryptedUsername.toLower();
        qry.prepare("SELECT username FROM players WHERE username=:f1");
        qry.bindValue(":f1", decryptedUsername);
        qry.exec();
        qry.next();
        if(!qry.value(0).isValid()){

            //The reason I create a player object and bind values is to make the code easier to edit in the future
            //I don't need the player object and it takes up some memory we don't have to consume, but
            //in the future it might be more difficult than necessary to make changes.
            Player player;
            player.experience = 0;
            player.guile = 0;
            player.intelligence = 0;
            player.strength = 0;
            player.resilience = 0;
            player.level = 1;
            player.miningattempts = 5;
            player.miningupgrades = 2;
            player.resetpoints = 5;
            player.statpoints = 15;
            player.money = 250;
            player.maxHP = 175;
            player.currentHP = 175;
            player.maxMana = 50;
            player.currentMana = 50;
            player.gold = 0;
            player.shards = 0;
            player.iron = 0;
            player.silicon = 0;
            player.sulfur = 0;
            player.goldLevel = 0;
            player.shardsLevel = 0;
            player.ironLevel = 0;
            player.siliconLevel = 0;
            player.sulfurLevel = 0;
            player.energy = 50.0;
            player.maxEnergy = 50.0;
            qry.prepare("INSERT INTO players (username, password, level, experience, strength, guile, intelligence, resilience, statpoints, resetpoints, miningattempts, miningupgrades, money, maxHP, currentHP, maxMana, currentMana, gold, shards, iron, silicon, sulfur, goldLevel, shardsLevel, ironLevel, siliconLevel, sulfurLevel, loggedIn, energy, maxEnergy) VALUES (:f1, :f2, :f3, :f4, :f5, :f6, :f7, :f8, :f9, :f10, :f11, :f12, :f13, :f14, :f15, :f16, :f17, :f18, :f19, :f20, :f21, :f22, :f23, :f24, :f25, :f26, :f27, :f28, :f29, :f30)");
            qry.bindValue(":f1", decrypt(data.stringData.at(0).toUtf8(), data.stringData.at(0).length()).toLower());
            qry.bindValue(":f2", data.stringData.at(1));
            qry.bindValue(":f3", player.level);
            qry.bindValue(":f4", player.experience);
            qry.bindValue(":f5", player.strength);
            qry.bindValue(":f6", player.guile);
            qry.bindValue(":f7", player.intelligence);
            qry.bindValue(":f8", player.resilience);
            qry.bindValue(":f9", player.statpoints);
            qry.bindValue(":f10", player.resetpoints);
            qry.bindValue(":f11", player.miningattempts);
            qry.bindValue(":f12", player.miningupgrades);
            qry.bindValue(":f13", player.money);
            qry.bindValue(":f14", player.maxHP);
            qry.bindValue(":f15", player.currentHP);
            qry.bindValue(":f16", player.maxMana);
            qry.bindValue(":f17", player.currentMana);
            qry.bindValue(":f18", player.gold);
            qry.bindValue(":f19", player.shards);
            qry.bindValue(":f20", player.iron);
            qry.bindValue(":f21", player.silicon);
            qry.bindValue(":f22", player.sulfur);
            qry.bindValue(":f23", player.goldLevel);
            qry.bindValue(":f24", player.shardsLevel);
            qry.bindValue(":f25", player.ironLevel);
            qry.bindValue(":f26", player.siliconLevel);
            qry.bindValue(":f27", player.sulfurLevel);
            qry.bindValue(":f28", 1);
            qry.bindValue(":f29", player.energy);
            qry.bindValue(":f30", player.maxEnergy);
            int index = clients.indexOf(client);
            usernames[index] = decryptedUsername;
            if(qry.exec()){
                respond(1, "regvalsucc", client);
                qDebug() << "[" << currentTime() << "] " << "C1: User" << decrypt(data.stringData.at(0).toUtf8(), data.stringData.at(0).length()) << "registered!";
            }
            else{
                respond(2, "regvalfail", client);
                qDebug() << "[" << currentTime() << "] " << "C2: User" << decrypt(data.stringData.at(0).toUtf8(), data.stringData.at(0).length()) << "registeration failed!";
            }
        }else{
            qDebug() << "[" << currentTime() << "] " << "C5: User" << decrypt(data.stringData.at(0).toUtf8(), data.stringData.at(0).length()) << "already exists";
            respond(5, "regvalfail", client);
        }
        return;
    }
    if(data.activityID == 4){ //user disconnect
        qDebug() << "[" << currentTime() << "] " << data.stringData.at(0);
        return;
    }
    if(data.activityID == 5){ //add strength point
        QString decryptedUsername = decrypt(data.stringData.at(0).toUtf8(), data.stringData.at(0).length());
        decryptedUsername = decryptedUsername.toLower();
        qry.prepare("UPDATE players SET strength=strength+1, statpoints=statpoints-1 WHERE username=:f3");
        qry.bindValue(":f3", decryptedUsername);
        qry.exec();
        return;
    }
    if(data.activityID == 6){ //add guile point
        QString decryptedUsername = decrypt(data.stringData.at(0).toUtf8(), data.stringData.at(0).length());
        decryptedUsername = decryptedUsername.toLower();
        qry.prepare("UPDATE players SET guile=guile+1, statpoints=statpoints-1 WHERE username=:f3");
        qry.bindValue(":f3", decryptedUsername);
        qry.exec();
        return;
    }
    if(data.activityID == 7){ //add intelligence point
        QString decryptedUsername = decrypt(data.stringData.at(0).toUtf8(), data.stringData.at(0).length());
        decryptedUsername = decryptedUsername.toLower();
        qry.prepare("UPDATE players SET intelligence=intelligence+1, statpoints=statpoints-1, currentMana=currentMana+3, maxMana=maxMana+3 WHERE username=:f3");
        qry.bindValue(":f3", decryptedUsername);
        qry.exec();
        return;
    }
    if(data.activityID == 8){ //add resilience point
        QString decryptedUsername = decrypt(data.stringData.at(0).toUtf8(), data.stringData.at(0).length());
        decryptedUsername = decryptedUsername.toLower();
        qry.prepare("UPDATE players SET resilience=resilience+1, statpoints=statpoints-1, maxHP=maxHP+7, currentHP=currentHP+7 WHERE username=:f3");
        qry.bindValue(":f3", decryptedUsername);
        qry.exec();
        return;
    }
    if(data.activityID == 9){ //subtract strength point
        QString decryptedUsername = decrypt(data.stringData.at(0).toUtf8(), data.stringData.at(0).length());
        decryptedUsername = decryptedUsername.toLower();
        qry.prepare("UPDATE players SET strength=strength-1, resetpoints=resetpoints-1, statpoints=statpoints+1 WHERE username=:f3");
        qry.bindValue(":f3", decryptedUsername);
        qry.exec();
        return;
    }
    if(data.activityID == 10){ //subtract guile point
        QString decryptedUsername = decrypt(data.stringData.at(0).toUtf8(), data.stringData.at(0).length());
        decryptedUsername = decryptedUsername.toLower();
        qry.prepare("UPDATE players SET guile=guile-1, resetpoints=resetpoints-1, statpoints=statpoints+1 WHERE username=:f3");
        qry.bindValue(":f3", decryptedUsername);
        qry.exec();
        return;
    }
    if(data.activityID == 11){ //subtract intelligence point
        QString decryptedUsername = decrypt(data.stringData.at(0).toUtf8(), data.stringData.at(0).length());
        decryptedUsername = decryptedUsername.toLower();
        qry.prepare("SELECT intelligence, resetpoints, statpoints, maxMana, currentMana FROM players WHERE username=:f1");
        qry.bindValue(":f1", decryptedUsername);
        qry.exec();
        qry.next();
        int intel = qry.value(0).toInt() - 1;
        int respts = qry.value(1).toInt() - 1;
        int stpts = qry.value(2).toInt() + 1;
        int mxmn = qry.value(3).toInt();
        int crmn = qry.value(4).toInt();
        if(mxmn > 3)
            mxmn -= 3;
        else
            mxmn = 0;
        if(crmn > 3)
            crmn -= 3;
        else
            crmn = 0;
        qry.prepare("UPDATE players SET intelligence=:f1, resetpoints=:f2, statpoints=:f3, currentMana=:f4, maxMana=:f5 WHERE username=:f6");
        qry.bindValue(":f1", intel);
        qry.bindValue(":f2", respts);
        qry.bindValue(":f3", stpts);
        qry.bindValue(":f4", crmn);
        qry.bindValue(":f5", mxmn);
        qry.bindValue(":f6", decryptedUsername);
        qry.exec();
        return;
    }
    if(data.activityID == 12){ //subtract resilience point
        QString decryptedUsername = decrypt(data.stringData.at(0).toUtf8(), data.stringData.at(0).length());
        decryptedUsername = decryptedUsername.toLower();
        qry.prepare("SELECT resilience, resetpoints, statpoints, maxHP, currentHP FROM players WHERE username=:f1");
        qry.bindValue(":f1", decryptedUsername);
        qry.exec();
        qry.next();
        int resil = qry.value(0).toInt() - 1;
        int respts = qry.value(1).toInt() - 1;
        int stpts = qry.value(2).toInt() + 1;
        int mxhp = qry.value(3).toInt();
        int crhp = qry.value(4).toInt();
        if(mxhp > 7)
            mxhp -= 7;
        else
            mxhp = 1;
        if(crhp > 7)
            crhp -= 7;
        else if(crhp < 7 && crhp != 0)
            crhp = 1;
        else
            crhp = 0;
        qry.prepare("UPDATE players SET resilience=:f1, resetpoints=:f2, statpoints=:f3, currentHP=:f4, maxHP=:f5 WHERE username=:f6");
        qry.bindValue(":f1", resil);
        qry.bindValue(":f2", respts);
        qry.bindValue(":f3", stpts);
        qry.bindValue(":f4", crhp);
        qry.bindValue(":f5", mxhp);
        qry.bindValue(":f6", decryptedUsername);
        qry.exec();
        return;
    }
    if(data.activityID == 13){ //add gold level
        QString decryptedUsername = decrypt(data.stringData.at(0).toUtf8(), data.stringData.at(0).length());
        qry.prepare("UPDATE players SET goldLevel=goldLevel+1, miningupgrades=miningupgrades-1 WHERE username=:f1");
        qry.bindValue(":f1", decryptedUsername);
        qry.exec();
        return;
    }
    if(data.activityID == 14){ //add shards level
        QString decryptedUsername = decrypt(data.stringData.at(0).toUtf8(), data.stringData.at(0).length());
        qry.prepare("UPDATE players SET shardsLevel=shardsLevel+1, miningupgrades=miningupgrades-1 WHERE username=:f1");
        qry.bindValue(":f1", decryptedUsername);
        qry.exec();
        return;
    }
    if(data.activityID == 15){ //add iron level
        QString decryptedUsername = decrypt(data.stringData.at(0).toUtf8(), data.stringData.at(0).length());
        qry.prepare("UPDATE players SET ironLevel=ironLevel+1, miningupgrades=miningupgrades-1 WHERE username=:f1");
        qry.bindValue(":f1", decryptedUsername);
        qry.exec();
        return;
    }
    if(data.activityID == 16){ //add silicon level
        QString decryptedUsername = decrypt(data.stringData.at(0).toUtf8(), data.stringData.at(0).length());
        qry.prepare("UPDATE players SET siliconLevel=siliconLevel+1, miningupgrades=miningupgrades-1 WHERE username=:f1");
        qry.bindValue(":f1", decryptedUsername);
        qry.exec();
        return;
    }
    if(data.activityID == 17){ //add sulfur level
        QString decryptedUsername = decrypt(data.stringData.at(0).toUtf8(), data.stringData.at(0).length());
        qry.prepare("UPDATE players SET sulfurLevel=sulfurLevel+1, miningupgrades=miningupgrades-1 WHERE username=:f1");
        qry.bindValue(":f1", decryptedUsername);
        qry.exec();
        return;
    }
    if(data.activityID == 18){ //sub gold level
        QString decryptedUsername = decrypt(data.stringData.at(0).toUtf8(), data.stringData.at(0).length());
        qry.prepare("UPDATE players SET goldLevel=goldLevel-1, miningupgrades=miningupgrades+1, resetpoints=resetpoints-1 WHERE username=:f1");
        qry.bindValue(":f1", decryptedUsername);
        qry.exec();
        return;
    }
    if(data.activityID == 19){ //sub shards level
        QString decryptedUsername = decrypt(data.stringData.at(0).toUtf8(), data.stringData.at(0).length());
        qry.prepare("UPDATE players SET shardsLevel=shardsLevel-1, miningupgrades=miningupgrades+1, resetpoints=resetpoints-1 WHERE username=:f1");
        qry.bindValue(":f1", decryptedUsername);
        qry.exec();
        return;
    }
    if(data.activityID == 20){ //sub iron level
        QString decryptedUsername = decrypt(data.stringData.at(0).toUtf8(), data.stringData.at(0).length());
        qry.prepare("UPDATE players SET ironLevel=ironLevel-1, miningupgrades=miningupgrades+1, resetpoints=resetpoints-1 WHERE username=:f1");
        qry.bindValue(":f1", decryptedUsername);
        qry.exec();
        return;
    }
    if(data.activityID == 21){ //sub silicon level
        QString decryptedUsername = decrypt(data.stringData.at(0).toUtf8(), data.stringData.at(0).length());
        qry.prepare("UPDATE players SET siliconLevel=siliconLevel-1, miningupgrades=miningupgrades+1, resetpoints=resetpoints-1 WHERE username=:f1");
        qry.bindValue(":f1", decryptedUsername);
        qry.exec();
        return;
    }
    if(data.activityID == 22){ //sub sulfur level
        QString decryptedUsername = decrypt(data.stringData.at(0).toUtf8(), data.stringData.at(0).length());
        qry.prepare("UPDATE players SET sulfurLevel=sulfurLevel-1, miningupgrades=miningupgrades+1, resetpoints=resetpoints-1 WHERE username=:f1");
        qry.bindValue(":f1", decryptedUsername);
        qry.exec();
        return;
    }
    if(data.activityID == 23){ //mine
       QString decryptedUsername = decrypt(data.stringData.at(0).toUtf8(), data.stringData.at(0).length());
       QString attemptsToUseS = data.stringData.at(1);
       int attemptsToUse = attemptsToUseS.toInt();
       qry.prepare("SELECT goldLevel, shardsLevel, ironLevel, siliconLevel, sulfurLevel, gold, shards, iron, silicon, sulfur, miningattempts FROM players WHERE username=:f1");
       qry.bindValue(":f1", decryptedUsername);
       qry.exec();
       qry.next();
       QList<int> mineLevels;
       QList<int> materialAmounts;
       mineLevels << qry.value(0).toInt() << qry.value(1).toInt() << qry.value(2).toInt() << qry.value(3).toInt() << qry.value(4).toInt();
       materialAmounts << qry.value(5).toInt() << qry.value(6).toInt() << qry.value(7).toInt() << qry.value(8).toInt() << qry.value(9).toInt();
       int availableAttempts = qry.value(10).toInt();
       if(attemptsToUse > availableAttempts)
       {
          respond(7, "minefailattfail", client); //Not enough attempts (likely packet tampering)
          qDebug() << "!*!*!*![" << currentTime() << "] " << "C7: User" << decryptedUsername << "seems to be packet editing!";
          qDebug() << "!*!*!*![" << currentTime() << "] " << "Attempts to use: " << attemptsToUse << "Available: " << availableAttempts;
          return;
       }
       double randomVal;

       for(int i = 1; i <= attemptsToUse; i++){
           randomVal = (double)((rand() % 499) + 500) / 100; //GOLD
           randomVal = pow(randomVal, ((mineLevels[0] / 87.0) + 0.60)) * mineLevels[0]/3 * 0.8745;
           materialAmounts[0] += round(randomVal);

           randomVal = (double)((rand() % 499) + 500) / 100; //SHARDS
           randomVal = pow(randomVal, ((mineLevels[1] / 87.0) + 0.60)) * mineLevels[1]/3 * 0.6233;
           materialAmounts[1] += round(randomVal);

           randomVal = (double)((rand() % 499) + 500) / 100; //IRON
           randomVal = pow(randomVal, ((mineLevels[2] / 87.0) + 0.60)) * mineLevels[2]/3 * 1.7428;
           materialAmounts[2] += round(randomVal);

           randomVal = (double)((rand() % 499) + 500) / 100; //SILICON
           randomVal = pow(randomVal, ((mineLevels[3] / 87.0) + 0.60)) * mineLevels[3]/3 * 1.2323;
           materialAmounts[3] += round(randomVal);

           randomVal = (double)((rand() % 499) + 500) / 100; //SULFUR
           randomVal = pow(randomVal, ((mineLevels[4] / 87.0) + 0.60)) * mineLevels[4]/3 * 2.0165;
           materialAmounts[4] += round(randomVal);
       }
       //Update Database
       qry.prepare("UPDATE players SET miningattempts=:f0, gold=:f1, shards=:f2, iron=:f3, silicon=:f4, sulfur=:f5 WHERE username=:f6");
       qry.bindValue(":f0", (availableAttempts - attemptsToUse));
       qry.bindValue(":f1", materialAmounts[0]);
       qry.bindValue(":f2", materialAmounts[1]);
       qry.bindValue(":f3", materialAmounts[2]);
       qry.bindValue(":f4", materialAmounts[3]);
       qry.bindValue(":f5", materialAmounts[4]);
       qry.bindValue(":f6", decryptedUsername);
       qry.exec();
       QStringList miningResults;
       miningResults << QString::number((availableAttempts - attemptsToUse)) << QString::number(materialAmounts[0]) << QString::number(materialAmounts[1]) << QString::number(materialAmounts[2]) << QString::number(materialAmounts[3]) << QString::number(materialAmounts[4]);
       respond(6, miningResults, 6, client);
       qDebug() << "[" << currentTime() << "] " << "C6: User" << decryptedUsername << "successfully mined.";
       return;
    }
    else if(data.activityID == 24){ //update request        
        QString decryptedUsername = decrypt(data.stringData.at(0).toUtf8(), data.stringData.at(0).length());
        QStringList responseData;
        qry.prepare("SELECT miningattempts, energy, maxEnergy FROM players WHERE username=:f1");
        qry.bindValue(":f1", decryptedUsername);
        qry.exec();
        qry.next();
        responseData << qry.value(0).toString() << QString::number(qry.value(1).toDouble()) << QString::number(qry.value(2).toDouble());
        respond(8, responseData, responseData.size(), client);
        return;
    }
    else if(data.activityID == 26){ //adventure energy used
        QString decryptedUsername = decrypt(data.stringData.at(0).toUtf8(), data.stringData.at(0).length());
        qry.prepare("SELECT energy, currentHP FROM players WHERE username=:f0");
        qry.bindValue(":f0", decryptedUsername);
        qry.exec();
        qry.next();
        if(qry.value(0).toInt() < 0.5){
            //Not enough energy to adventure.
            respond(11, "noenergy", client);
            return;
        }

        else if(qry.value(1).toInt() <= 0){
            //Not enough health to adventure
            respond(36, "nohp", client);
            return;
        }
        else{
            //consume energy to adventure
            qry.prepare("UPDATE players SET energy=energy-0.5 WHERE username=:f0");
            qry.bindValue(":f0", decryptedUsername);
            qry.exec();  

            //populate variables with current user stats to save server time
            qry.prepare("SELECT currentHP, currentMana, maxHP, maxMana, money, maxEnergy, energy FROM players WHERE username=:f0");
            qry.bindValue(":f0", decryptedUsername);
            qry.exec();

            struct playerStruct{
                int currentHP;
                int currentMana;
                int maxHP;
                int maxMana;
                int money;
                int maxEnergy;
                int energy;
            };

            playerStruct currentPlayer;

            currentPlayer.currentHP   = qry.value(0).toInt();
            currentPlayer.currentMana = qry.value(1).toInt();
            currentPlayer.maxHP       = qry.value(2).toInt();
            currentPlayer.maxMana     = qry.value(3).toInt();
            currentPlayer.money       = qry.value(4).toInt();
            currentPlayer.maxEnergy   = qry.value(5).toInt();
            currentPlayer.energy      = qry.value(6).toInt();
            
            //calculate an event
            int randomVal;
            randomVal = (int)(rand() % 1000);
            
            //determine the type of event: good, bad, or neutral
            if(randomVal >= 0 && randomVal <= 750){ //75% chance of neutral event
                respond(12, "neutral", client);
                return;
            }
            else if(randomVal >= 751 && randomVal <= 850){ //10% chance of bad event
                randomVal = (int)(rand() % 100); //0-99
                if(randomVal >= 0 && randomVal <= 30){         //30% lose 1 HP
                    if(currentPlayer.currentHP <= 1){
                        //Player death
                        qry.prepare("UPDATE players SET currentHP=0 WHERE username=:f0");
                        qry.bindValue(":f0", decryptedUsername);
                        qry.exec();
                        respond(13, "-1hp", client);
                        respond(0, "death", client);
                        return;
                    }
                    else{
                        qry.prepare("UPDATE players SET currentHP=currentHP-1 WHERE username=:f0");
                        qry.bindValue(":f0", decryptedUsername);
                        qry.exec();
                        respond(13, "-1hp", client);
                        return;
                    }
                }
                else if(randomVal > 30 && randomVal <= 45){    //15% lose 40 mana 1 energy
                    if(currentPlayer.currentMana <= 40 && currentPlayer.energy <= 1){
                        qry.prepare("UPDATE players SET energy=0, currentMana=0 WHERE username=:f0");
                        qry.bindValue(":f0", decryptedUsername);
                        qry.exec();
                        respond(14, "-40mana-1energy", client);
                        return;
                    }
                    else if(currentPlayer.currentMana <= 40 && currentPlayer.energy > 1){
                        qry.prepare("UPDATE players SET energy=energy-1, currentMana=0 WHERE username=:f0");
                        qry.bindValue(":f0", decryptedUsername);
                        qry.exec();
                        respond(14, "-40mana-1energy", client);
                        return;
                    }
                    else if(currentPlayer.currentMana > 40 && currentPlayer.energy <= 1){
                        qry.prepare("UPDATE players SET energy=0, currentMana=currentMana-40 WHERE username=:f0");
                        qry.bindValue(":f0", decryptedUsername);
                        qry.exec();
                        respond(14, "-40mana-1energy", client);
                        return;
                    }
                    else{
                        qry.prepare("UPDATE players SET energy=energy-1, currentMana=currentMana-40 WHERE username=:f0");
                        qry.bindValue(":f0", decryptedUsername);
                        qry.exec();
                        respond(14, "-40mana-1energy", client);
                        return;
                    }
                }
                else if(randomVal > 45 && randomVal <= 60){    //15% lose 10 hp 1 energy
                    if(currentPlayer.currentHP <= 10 && currentPlayer.energy <= 1){
                        qry.prepare("UPDATE players SET energy=0, currentHP=0 WHERE username=:f0");
                        qry.bindValue(":f0", decryptedUsername);
                        qry.exec();
                        respond(15, "-10hp-1energy", client);
                        return;
                    }
                    else if(currentPlayer.currentHP <= 10 && currentPlayer.energy > 1){
                        qry.prepare("UPDATE players SET energy=energy-1, currentHP=0 WHERE username=:f0");
                        qry.bindValue(":f0", decryptedUsername);
                        qry.exec();
                        respond(15, "-10hp-1energy", client);
                        return;
                    }
                    else if(currentPlayer.currentHP > 10 && currentPlayer.energy <= 1){
                        qry.prepare("UPDATE players SET energy=0, currentHP=currentHP-10 WHERE username=:f0");
                        qry.bindValue(":f0", decryptedUsername);
                        qry.exec();
                        respond(15, "-10hp-1energy", client);
                        return;
                    }
                    else{
                        qry.prepare("UPDATE players SET energy=energy-1, currentHP=currentHP-1 WHERE username=:f0");
                        qry.bindValue(":f0", decryptedUsername);
                        qry.exec();
                        respond(15, "-10hp-1energy", client);
                        return;
                    }
                }
                else if(randomVal > 60 && randomVal <= 70){    //10% lose 20 hp
                    if(currentPlayer.currentHP <= 20){
                        qry.prepare("UPDATE players SET currentHP=0 WHERE username=:f0");
                        qry.bindValue(":f0", decryptedUsername);
                        qry.exec();
                        respond(16, "-20hp", client);
                        respond(0, "death", client);
                        return;
                    }
                    else{
                        qry.prepare("UPDATE players SET currentHP=currentHP-20 WHERE username=:f0");
                        qry.bindValue(":f0", decryptedUsername);
                        qry.exec();
                        respond(16, "-20hp", client);
                        return;
                    }
                }
                else if(randomVal > 70 && randomVal <= 80){ //10% lose 100 gold
                    if(currentPlayer.money > 100){
                        qry.prepare("UPDATE players SET money=money-100 WHERE username=:f0");
                        qry.bindValue(":f0", decryptedUsername);
                        qry.exec();
                        respond(17, "-100money", client);
                        return;
                    }
                    else{
                        qry.prepare("UPDATE players SET money=0 WHERE username=:f0");
                        qry.bindValue(":f0", decryptedUsername);
                        qry.exec();
                        respond(17, "-100money", client);
                        return;
                    }
                }
                else if(randomVal > 80 && randomVal <= 90){ //10% lose 30 hp 25 mana
                    if(currentPlayer.currentHP <= 30){
                        qry.prepare("UPDATE players SET currentHP=0 WHERE username=:f0");
                        qry.bindValue(":f0", decryptedUsername);
                        qry.exec();
                        respond(18, "-30hp-25mana", client);
                        return;
                    }
                    else{
                        qry.prepare("UPDATE players SET currentHP=currentHP-30 WHERE username=:f0");
                        qry.bindValue(":f0", decryptedUsername);
                        qry.exec();
                        respond(18, "-30hp-25mana", client);
                        return;
                    }

                    if(currentPlayer.currentMana <= 25){
                        qry.prepare("UPDATE players SET currentMana=0 WHERE username=:f0");
                        qry.bindValue(":f0", decryptedUsername);
                        qry.exec();
                        respond(18, "-30hp-25mana", client);
                        return;
                    }
                    else{
                        qry.prepare("UPDATE players SET currentMana=currentMana-25 WHERE username=:f0");
                        qry.bindValue(":f0", decryptedUsername);
                        qry.exec();
                        respond(18, "-30hp-25mana", client);
                        return;
                    }
                }
                else{                                       //5% lose 5 energy
                    if(currentPlayer.energy <= 5){
                        qry.prepare("UPDATE players SET energy=0 WHERE username=:f0");
                        qry.bindValue(":f0", decryptedUsername);
                        qry.exec();
                        respond(19, "-5energy", client);
                        return;
                    }
                    else{
                        qry.prepare("UPDATE players SET energy=energy-5 WHERE username=:f0");
                        qry.bindValue(":f0", decryptedUsername);
                        qry.exec();
                        respond(19, "-5energy", client);
                        return;
                    }
                }
            }
            else if(randomVal >= 851 && randomVal <= 900){ //5% chance of good event
                
            }
            else if(randomVal > 900){ //10% chance of combat
                
            }
        }
    }
}

void Server::createTable(){
    QSqlQuery qry;
    ifstream ifile(PATH_TO_DB);
    qry.prepare("CREATE TABLE players (id INTEGER PRIMARY KEY, username TEXT, password TEXT, level NUMERIC, experience NUMERIC, strength NUMERIC, guile NUMERIC, intelligence NUMERIC, resilience NUMERIC, statpoints NUMERIC, resetpoints NUMERIC, miningattempts NUMERIC, miningupgrades NUMERIC, money NUMERIC, maxHP NUMERIC, currentHP NUMERIC, maxMana NUMERIC, currentMana NUMERIC, gold NUMERIC, shards NUMERIC, iron NUMERIC, silicon NUMERIC, sulfur NUMERIC, goldLevel NUMERIC, shardsLevel NUMERIC, ironLevel NUMERIC, siliconLevel NUMERIC, sulfurLevel NUMERIC, loggedIn NUMERIC, energy REAL, maxEnergy REAL)");
    qry.exec();
    if (ifile) {
        qDebug() << "[" << currentTime() << "] " << "Database found.";
    }
    else{
        qDebug() << "[" << currentTime() << "] " << "Database not found.";
    }
}

void Server::respond(int activityID, QStringList stringData, int stringCount, QTcpSocket *client){
    QByteArray serverSendBuf;
    receivedData response;
    response.activityID = activityID;
    for(int i = 0; i < stringCount; i++){
        response.stringData << stringData.at(i);
    }
    response.stringCount = stringCount;
    for(int i = 0; i < stringCount; i++){
        response.stringLength.append(stringData.at(i).length());
    }
    serverSendBuf.append( (char) response.activityID); //populating the send buffer
    serverSendBuf.append( (char) response.stringCount);
    for (int i = 0; i < response.stringLength.size(); i++){
        serverSendBuf.append( (char) response.stringLength[i]);
    }
    for (int j = 0; j < stringCount; j++){
        for (int i = 0; i < response.stringData[j].length(); i++){
            serverSendBuf.append(response.stringData[j].at(i));
        }
    }

    // Header
    uint16_t messageSize = serverSendBuf.size();
    char *bytes((char*)&messageSize);
    serverSendBuf.prepend(bytes, sizeof(messageSize));

    client->write(serverSendBuf);
}

void Server::respond(int activityID, QString stringData, QTcpSocket *client){
    QByteArray serverSendBuf;
    receivedData response;
    response.activityID = activityID;
    response.stringData << stringData;
    response.stringCount = 1;
    response.stringLength.append(stringData.length());
    serverSendBuf.append( (char) response.activityID); //populating the send buffer
    serverSendBuf.append( (char) response.stringCount);
    serverSendBuf.append( (char) response.stringLength[0]);
    for (int i = 0; i < response.stringData[0].length(); i++){
        serverSendBuf.append(response.stringData[0].at(i));
    }

    // Header
    uint16_t messageSize = serverSendBuf.size();
    char *bytes((char*)&messageSize);
    serverSendBuf.prepend(bytes, sizeof(messageSize));

    client->write(serverSendBuf);
}

QByteArray Server::encrypt(QString input){
    std::string original = input.toStdString();
    std::string encrypted = "";

    std::string key = "862384";

    for (unsigned int temp = 0; temp < original.size(); temp++){
        encrypted += original[temp] ^ ((atoi(key.c_str()) + temp) + 2) % 253;
    }

    QByteArray byteArray(encrypted.c_str(), encrypted.length());
    return byteArray;
}

QByteArray Server::encrypt(char* input){
    std::string original = input;
    std::string encrypted = "";

    std::string key = "862384";

    for (unsigned int temp = 0; temp < original.size(); temp++){
        encrypted += original[temp] ^ ((atoi(key.c_str()) + temp) + 2) % 253;
    }

    QByteArray byteArray(encrypted.c_str(), encrypted.length());
    return byteArray;
}

QString Server::decrypt(QByteArray input, unsigned int inputLength){

    std::string unencrypt = "";
    std::string key = "862384";

    for (unsigned int temp = 0; temp < inputLength; temp++){
    unencrypt += input[temp] ^ ((atoi(key.c_str()) + temp) + 2) % 253;
    }
    QString output = QString::fromStdString(unencrypt);

    return output;

}

QString Server::currentTime() {
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    strftime(buf, sizeof(buf), "%X", &tstruct);

    return buf;
}

void Server::delay( int millisecondsToWait )
{
    QTime dieTime = QTime::currentTime().addMSecs( millisecondsToWait );
    while( QTime::currentTime() < dieTime )
    {
        QCoreApplication::processEvents( QEventLoop::AllEvents, 100 );
    }
}
