Client Send IDs:

0  ; Player death
1  ; Client greeting
2  ; User login
3  ; User registration
4  ; User disconnect
5  ; Add Strength
6  ; Add Guile
7  ; Add Intelligence
8  ; Add Resilience
9  ; Subtract Strength
10 ; Subtract Guile
11 ; Subtract Intelligence
12 ; Subtract Resilience
13 ; Add Gold Level
14 ; Add Shards Level
15 ; Add Iron Level
16 ; Add Silicon Level 
17 ; Add Sulfur Level
18 ; Sub Gold Level
19 ; Sub Shards Level
20 ; Sub Iron Level
21 ; Sub Silicon Level
22 ; Sub Sulfur Level
23 ; Mine
24 ; Update Request
25 ; Client is Alive
26 ; Adventure action performed 

Server Response IDs:

0  ; Player death
1  ; Registration validation success
2  ; Registration validation failure
3  ; Login validation success
4  ; Login validation failure
5  ; Username already exists (During registration only)
6  ; Mining successful (contains results of the mining)
7  ; Mining Unsuccessful (not enough attempts)
8  ; Update tick
9  ; Client Life Check
10 ; User already logged in
11 ; Not enough energy to adventure
12 ; Something Neutral (generate flavor text client-side)
===Something Bad===
13 ; Lose 1 HP
14 ; Lose 10 mana 1 energy
15 ; Lose 10 hp 1 energy
16 ; Lose 20 hp
17 ; Lose 100 gold
18 ; Lose 10 hp 10 mana
19 ; Lose 2 energy
===Something Good===
20 ; Gain 5 HP
21 ; Gain 1 energy
22 ; Gain 1 mana
23 ; Gain 10 gold
24 ; Gain 5 energy
25 ; Gain 250 gold
26 ; Gain 10 energy
27 ; Gain 30 HP
28 ; Gain 1 Max HP
29 ; Gain 50 experience
30 ; Gain 1 mining attempt
31 ; Gain 1 mining upgrade
32 ; Gain 1 reset point
33 ; Gain 1 max mana
34 ; Gain 1-20 of a random resource
35 ; Gain 0.5 max energy
===Combat===
 ; Combat